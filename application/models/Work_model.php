<?php

class Work_model extends MyBase_model 
{
    public $columns = [
        'work',
        'description',
        'year',
        'city',
        'position',
    ];

    
    public function __construct() {
        $this->table = 'works';
    }

    public function get($id)
    {
        $work = objectToArray(parent::get($id));

        $categoriesId = $this->db->select('category_id')
            ->where('work_id', $id)
            ->get('work_categories')
            ->result_array();
        $categoriesId = array_column($categoriesId, 'category_id');
        $work['categories'] = $categoriesId;

        return $work;
    }

    public function getCategories($id)
    {
        $sql = "SELECT work_id, GROUP_CONCAT(name) categories
            FROM work_categories wc 
            JOIN categories c ON wc.category_id = c.id
            WHERE wc.work_id = ?
            GROUP BY wc.work_id";
        return $this->db->query($sql, [$id])->row();
    }

    public function getGeneral()
    {        
        $works = $this->db->select('id, work, year, city, position, description')->get($this->table)->result_array();
        $works = setValueAsKey('id', $works);
        
        $categoriesId = $this->db->select('category_id')
            ->distinct()
            ->get('work_categories')
            ->result_array();
        $categoriesId = array_column($categoriesId, 'category_id');

        $categories = $this->db->where_in($categoriesId)
            ->get('categories')
            ->result_array();
        $categories = setValueAsKey('id', $categories);

        $workCategories = $this->db->get('work_categories')->result_array();

        foreach($works as $key => $work) {
            $works[$key]['categories'] = [];
        }

        foreach($workCategories as $workCategory) {
            $works[$workCategory['work_id']]['categories'][] = $categories[$workCategory['category_id']];
        }

        return array_values($works);
    }

    public function store($data) :bool 
    {
        $work = inputToArrayAssoc($data, $this);
        $categoriesId = $data['categories_id'];
        
        $this->db->trans_start();
        $this->db->insert($this->table, $work);
        $workId = $this->db->insert_id();
        $workCategories = array_map(function($category) use ($workId) {
            return [
                'category_id'   => $category,
                'work_id'       => $workId,
            ];
        }, $categoriesId);
        $this->db->insert_batch('work_categories', $workCategories);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function update($id, $data) :bool
    {
        $work = inputToArrayAssoc($data, $this);
        $categoriesId = $data['categories_id'];

        $this->db->trans_start();
        $this->db->where('work_id', $id)->delete('work_categories');
        parent::update($id, $work);
        $workCategories = array_map(function($category) use ($id) {
            return [
                'category_id'   => $category,
                'work_id'       => $id,
            ];
        }, $categoriesId);
        $this->db->insert_batch('work_categories', $workCategories);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    public function delete($id)
    {
        $this->db->trans_start();
        $this->db->where('work_id', $id)->delete('work_categories');
        parent::delete($id);
        $this->db->trans_complete();
        return $this->db->trans_status();

    }

    public function getLastCreated($limit = 2) 
    {
        $sql = "SELECT * FROM $this->table w 
            JOIN (
                SELECT work_id as id, GROUP_CONCAT(name SEPARATOR ', ') category
                FROM work_categories wc	
                JOIN categories c	
                ON wc.category_id = c.id
                GROUP BY work_id
                ) gc
            ON w.id = gc.id
            ORDER BY created_at DESC
            LIMIT $limit";
        return $this->db
            ->query($sql)
            ->result();
    }
}