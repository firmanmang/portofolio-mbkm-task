<?php
class Skill_model extends MyBase_model 
{
    public $columns = [
        'name',
        'level',
    ];

    public function __construct() {
        parent::__construct();

        $this->table = 'skills';
    }
}