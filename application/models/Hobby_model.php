<?php
class Hobby_model extends MyBase_model 
{
    public $columns = [
        'hobby',
    ];

    public function __construct() {
        parent::__construct();

        $this->table = 'hobies';
    }
}