<?php
class Language_model extends MyBase_model 
{
    public $columns = [
        'language',
        'level',
    ];

    public function __construct() {
        parent::__construct();

        $this->table = 'languages';
    }
}