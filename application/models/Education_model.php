<?php
class Education_model extends MyBase_model 
{
    public $columns = [
        'name',
        'start_date',
        'graduate_date',
        'description',
    ];

    public function __construct() {
        parent::__construct();

        $this->table = 'educations';
    }
}