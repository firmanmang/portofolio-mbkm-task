<?php

class MyBase_model extends CI_Model 
{
    protected $table;
    
    public function getAll($columns = '*')
    {
        return $this->db->select($columns)
            ->get($this->table)
            ->result();
    }

    public function get($id)
    {
        return $this->db->where('id', $id)
                ->get($this->table)
                ->row();
    }

    public function pick($columns = '*', $conditions = [])
    {
        return $this->db->select($columns)
            ->where($conditions)
            ->get($this->table)
            ->result();
    }

    public function update($id, $data): bool
    {
        return $this->db->where('id', $id)
            ->update($this->table,$data);
    }

    public function store($data): bool
    {
        return $this->db->insert($this->table, $data);
    }

    public function delete($id)
    {
        return $this->db->where('id', $id)
            ->delete($this->table);
    }

    public function getForDatatable($parameters)
    {
        $columns            = $parameters['columns'];
        $limit              = $parameters['length'];
        $offset             = $parameters['start'];
        $searchValue        = $parameters['search']['value'];
        $columnsName        = [];
        $searchableColumns  = [];
        $orderableColumns   = $parameters['order'];
        $orderQuery         = $this->getOrderQueryDatatable($columns, $orderableColumns);
        
        foreach($columns as $column) {
            $columnsName[] = $column['data'];
            if ($column['searchable'] == 'true') {
                $searchableColumns[] = $column['data'];
            }
        }

        $columnsName    = implode(', ', $columnsName);
        $searchQuery    = ($searchValue != null) 
                ? $this->getSearchQueryDatatable($searchValue, $searchableColumns)
                : '';
                
        $sqlGetFiltered = "SELECT $columnsName FROM $this->table $searchQuery ORDER BY $orderQuery LIMIT $limit OFFSET $offset";
        $sqlGetNumRow = "SELECT COUNT(id) as total FROM $this->table $searchQuery";

        $data           = $this->db->query($sqlGetFiltered)->result();
        $recordsFiltered= $this->db->query($sqlGetNumRow)->row()->total;
        $recordsTotal   = $this->db->count_all($this->table);

        $result = [
            'draw'              => $parameters['draw'],
            'recordsTotal'      => $recordsTotal,
            'recordsFiltered'   => $recordsFiltered,
            'data'              => $data,
        ];

        return $result;
    }

    protected  function getSearchQueryDatatable($searchValue, $searchableColumns)
    {
        $searchableColumns = array_map(function($searchColumn) 
                use ($searchValue) {
            return $searchColumn.' LIKE \'%'.$searchValue.'%\' ESCAPE \'!\'';
        }, $searchableColumns);
        $searchQuery = 'WHERE '.implode(' OR ', $searchableColumns);
        return $searchQuery;
    }

    protected function getOrderQueryDatatable($columns, $orderableColumns) {
        $orderableColumns = array_map(function($orderColumn) use ($columns) {
            return $columns[$orderColumn['column']]['data']
                    . ' '. strtoupper($orderColumn['dir']);
        }, $orderableColumns);
        $orderQuery = implode(', ', $orderableColumns);
        return $orderQuery;
    }

    public function storeBatch($data)
    {
        return $this->db->insert_batch($this->table, $data);
    }
}