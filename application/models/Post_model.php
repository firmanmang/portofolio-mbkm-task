<?php

class Post_model extends MyBase_model 
{
    public $columns = [
        'title',
        'slug',
        'category_id',
        'content',
        'img_feature',
        'created_by',
    ];

    private $sql;

    public function __construct() {
        $this->table = 'posts';
        $this->sql = "SELECT p.id id, title, slug, img_feature, category_name, name as writer
            FROM {$this->table} p 
            JOIN post_categories pc 
                ON p.category_id = pc.id
            JOIN user u
                ON p.created_by = u.id";
    }

    public function get($id)
    {
        return $this->db
            ->select($this->table.'.id as id, title, slug, category_id, content, img_feature, created_at, created_by, last_modified, category_name')
            ->where($this->table.'.id', $id)
            ->join('post_categories', 'post_categories.id = '.$this->table.'.category_id')
            ->get($this->table)->row();
    }

    public function getGeneral()
    {
        
        return $this->db->query($this->sql)->result();
    }

    public function findSlug($slug)
    {
        return $this->db->where('slug', $slug)->get($this->table)->result();
    }


    public function getForDatatable($parameters)
    {
        $columns            = $parameters['columns'];
        $limit              = $parameters['length'];
        $offset             = $parameters['start'];
        $searchValue        = $parameters['search']['value'];
        $searchableColumns  = [];
        $orderableColumns   = $parameters['order'];

        foreach($columns as $column) {
            $columnsName[] = $column['data'];
            if ($column['searchable'] == 'true') {
                $searchableColumns[] = $column['data'];
            }
        }

        $orderQuery         = $this->getOrderQueryDatatable($columns, $orderableColumns);
        $orderQuery = str_replace('id', 'u.id', $orderQuery);
        $orderQuery = str_replace('writer', 'u.name', $orderQuery);

        $searchQuery        = ($searchValue != null) 
                ? $this->getSearchQueryDatatable($searchValue, $searchableColumns)
                : '';
        $searchQuery = str_replace('id', 'u.id', $searchQuery);
        $searchQuery = str_replace('writer', 'u.name', $searchQuery);
        
        $sqlGetNumRow = $this->sql.' '.$searchQuery;
        $sqlGetFiltered = $sqlGetNumRow." ORDER BY $orderQuery LIMIT $limit OFFSET $offset";

        $data           = $this->db->query($sqlGetFiltered)->result();
        $recordsFiltered= count($this->db->query($sqlGetNumRow)->result());
        $recordsTotal   = $this->db->count_all($this->table);

        $result = [
            'draw'              => $parameters['draw'],
            'recordsTotal'      => $recordsTotal,
            'recordsFiltered'   => $recordsFiltered,
            'data'              => $data,
        ];

        return $result;
    }

    public function getLastCreated($limit = 2) 
    {
        return $this->db
            ->select($this->table.'.id as id, title, slug, category_id, content, img_feature, created_at, created_by, last_modified, category_name')
            ->join('post_categories', $this->table.'.category_id = post_categories.id' )
            ->order_by('created_at', 'desc')
            ->get($this->table, $limit)
            ->result();
    }
}