<?php

class Workcategory_model extends MyBase_model 
{   
    public $columns = [
        'name',
    ];

    public function __construct() {
        $this->table = 'categories';
    }

    public function getAll($columns = '*')
    {
        $categories = $this->db->select($columns)
            ->get($this->table)
            ->result_array();
        $categories = setValueAsKey('id', $categories);

        $works = $this->db->select('id, work')->get('works')->result_array();
        $works = setValueAsKey('id', $works);

        $workCategories = $this->db->select('category_id, work_id')
            ->get('work_categories')
            ->result_array();
        
        foreach($categories as $key => $category) {
            $categories[$key]['works'] = [];
        }

        foreach($workCategories as $workCategory) {
            $categories[$workCategory['category_id']]['works'][] = $works[$workCategory['work_id']];
        }

        $categories = array_values($categories);
        return $categories;
        
    }
    
    // public function delete($id)
    // {
    //     return $this->db->where('id', $id)
    //         ->update($this->table, [
    //             'is_active' => 0
    //         ]);
    // }
}