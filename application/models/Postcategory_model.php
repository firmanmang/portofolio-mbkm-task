<?php

class Postcategory_model extends MyBase_model 
{   
    public $columns = [
        'category_name',
        'is_active',
    ];

    public function __construct() {
        $this->table = 'post_categories';
    }
    
    public function getActive($columns = '*')
    {
        return $this->db->select($columns)
            ->where('is_active', '1')
            ->get($this->table)
            ->result();
    }

    public function update($id, $data): bool
    {
        $isUsed = ($this->db->select('category_id')
            ->where('category_id', $id)
            ->get('posts')
            ->num_rows() > 0);
            
        if ($isUsed) {
            return false;
        }

        return parent::update($id, $data);
    }

    // public function delete($id)
    // {
    //     return $this->db->where('id', $id)
    //         ->update($this->table, [
    //             'is_active' => 0
    //         ]);
    // }
}