<?php
class Experience_model extends MyBase_model 
{
    public $columns = [
        'name',
        'start_date',
        'resign_date',
        'description',
    ];

    public function __construct() {
        parent::__construct();

        $this->table = 'experiences';
    }
}