<?php

class User_model extends MyBase_model {

    public $columns = [
        'name',
        'username',
        'email',
        'phone_number',
        'password',
        'provinsi_id',
        'kabupaten_id',
        'kecamatan_id',
        'desa_id',
    ];

    public function __construct() {
        $this->table = 'user';
    }

    public function register($user) : bool
    {
        if ($this->db
                ->where(['username' => $user['username']])
                ->where(['email' => $user['email']])
                ->get($this->table)
                ->num_rows() > 0) {
            return false;
        }

        $user['password'] = password_hash($user['password'], PASSWORD_BCRYPT);

        $this->db->insert($this->table, $user);
        return true;
    }

    public function login($user)
    {
        $query = $this->db
            ->where(['username' => $user['identifier']])
            ->or_where(['email' => $user['identifier']])
            ->get($this->table);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {   
                if (password_verify($user['password'], $row->password)) {
                    return $query->row();
                }
            }
        }

        return null;
    }


    public function get($id)
    {
        return $this->db
            ->select('name, image_profile, username, email, phone_number, provinsi_id, kabupaten_id, kecamatan_id, desa_id')
            ->where('id', $id)
            ->get($this->table)
            ->row();
    }

    public function isExist($column, $value)
    {
        return $this->db->where($column, $value)
            ->get($this->table)
            ->num_rows() > 0;
    }

    public function changePassword($id, $data)
    {
        $query = $this->db->where('id', $id);
        $user = $query->get($this->table)->row();

        if (! password_verify($data['current_password'], $user->password)) {
            return false;
        }

        $query->update($this->table, [
            'password' => password_hash($data['new_password'], PASSWORD_BCRYPT),
        ]);

        return true;
    }
}