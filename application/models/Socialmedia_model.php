<?php
class Socialmedia_model extends MyBase_model 
{
    public $columns = [
        'name',
        'url',
    ];

    public function __construct() {
        parent::__construct();

        $this->table = 'social_medias';
    }
}