<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <title>Post Category</title>
    <!--     Fonts and icons     -->
    <!---    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />    --->
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="/assets/fonts/fontawesome/css/font-awesome.min.css">
    <!-- Nucleo Icons -->
    <link href="/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Popper -->
    <!-- <script src="https://unpkg.com/@popperjs/core@2"></script> -->
    <!-- Main Styling -->
    <link href="/assets/css/argon-dashboard-tailwind.css" rel="stylesheet" />
    
    <script src="/assets/js/jquery-1.min.js"></script>
    <script src="/assets/js/swal2.min.js"></script>
    <?php foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    
    <?php endforeach; ?>
    <?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
        
    <?php endforeach; ?>
 
    
</head>

<body class="m-0 font-sans text-base antialiased font-normal dark:bg-slate-900 leading-default bg-gray-50 text-slate-500">
    <div class="absolute w-full bg-blue-500 dark:hidden min-h-75"></div>
    <?php component('sidenav') ?>

    <main class="relative h-full max-h-screen transition-all duration-200 ease-in-out xl:ml-68 rounded-xl">

        <?php component('navbar', ['title' => 'post']) ?>
        <!-- cards -->
        <div class="min-h-screen w-full px-6 py-6 mx-auto">
            <div class="flex flex-wrap -mx-3">
                <div class="flex-none w-full max-w-full px-3">
                    <div class="relative flex flex-col min-w-0 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
                        <!-- Tabel Content -->
                        <?= $output ?>
                        <!-- End Tabel Content -->

                    </div>
                </div>
            </div>

            <?php component('footer') ?>
        </div>
        <!-- end cards -->
    </main>

    <?php component('right-conf') ?>

</body>
<!-- plugin for charts  -->
<script src="/assets/js/plugins/chartjs.min.js"></script>
<!-- plugin for scrollbar  -->
<script src="/assets/js/plugins/perfect-scrollbar.min.js"></script>
<!-- main script file  -->
<script src="/assets/js/argon-dashboard-tailwind.js?v=1.0.1"></script>

</html>