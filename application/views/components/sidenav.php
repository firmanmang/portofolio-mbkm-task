<!-- sidenav  -->
<aside class="fixed inset-y-0 flex-wrap items-center justify-between block w-full p-0 my-4 overflow-y-auto antialiased transition-transform duration-200 -translate-x-full bg-white border-0 shadow-xl dark:shadow-none dark:bg-slate-850 max-w-64 ease-nav-brand z-990 xl:ml-6 rounded-2xl xl:left-0 xl:translate-x-0" aria-expanded="false">
    <div class="h-19">
        <i class="absolute top-0 right-0 p-4 opacity-50 cursor-pointer fa fa-times dark:text-white text-slate-400 xl:hidden" sidenav-close></i>
        <a class="block px-8 py-6 m-0 text-sm whitespace-nowrap dark:text-white text-slate-700" href="<?= site_url() ?>" target="_blank">
            <img src="/assets/img/logo-ct-dark.png" class="inline h-full max-w-full transition-all duration-200 dark:hidden ease-nav-brand max-h-8" alt="main_logo" />
            <img src="/assets/img/logo-ct.png" class="hidden h-full max-w-full transition-all duration-200 dark:inline ease-nav-brand max-h-8" alt="main_logo" />
            <span class="ml-1 font-semibold transition-all duration-200 ease-nav-brand">Portofolio</span>
        </a>
    </div>

    <hr class="h-px mt-0 bg-transparent bg-gradient-to-r from-transparent via-black/40 to-transparent dark:bg-gradient-to-r dark:from-transparent dark:via-white dark:to-transparent" />

    <div class="items-center block w-auto max-h-screen overflow-auto grow basis-full">
        <ul class="flex flex-col pl-0 mb-0 h-full">
            <li class="mt-0.5 w-full">
                <a href="<?= site_url('dashboard') ?>" class="py-2.7 <?php if (isActive('dashboard')) echo 'bg-blue-500/20' ?> hover:bg-blue-500/30 dark:text-white dark:opacity-80 text-sm ease-nav-brand my-0 mx-2 flex items-center whitespace-nowrap rounded-lg px-4 font-semibold text-slate-700 transition-colors">
                    <div class="mr-2 flex h-8 w-8 items-center justify-center rounded-lg bg-center stroke-0 text-center xl:p-2.5">
                        <i class="text-blue-500 fa fa-television"></i>
                    </div>
                    <span class="ml-1 duration-300 opacity-100 pointer-events-none ease">Dashboard</span>
                </a>
            </li>
            <li class="mt-0.5 w-full">
                <div class="py-2.7 dark:text-white dark:opacity-80 text-sm ease-nav-brand my-0 mx-2 flex items-center whitespace-nowrap rounded-lg px-4 font-semibold text-slate-700 transition-colors">
                    <div class="mr-2 flex h-8 w-8 items-center justify-center rounded-lg bg-center stroke-0 text-center xl:p-2.5">
                        <i class="text-gray-500 fa fa-th-large"></i>
                    </div>
                    <span class="ml-1 duration-300 opacity-100 pointer-events-none ease">Category</span>
                </div>
                <ul class="ml-3">
                    <li>
                        <a href="<?= site_url('admin/postcategory') ?>" class="py-2.7 <?php if (isActive('admin/postcategory')) echo 'bg-blue-500/20' ?> hover:bg-blue-500/30 dark:text-white dark:opacity-80 text-sm ease-nav-brand my-0 mx-2 flex items-center whitespace-nowrap rounded-lg px-4 font-semibold text-slate-700 transition-colors">
                            <div class="mr-2 flex h-8 w-8 items-center justify-center rounded-lg bg-center stroke-0 text-center xl:p-2.5">
                                <i class="text-green-500 fa fa-list-alt"></i>
                            </div>
                            <span class="ml-1 duration-300 opacity-100 pointer-events-none ease">Post Categories</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('admin/workcategory') ?>" class="py-2.7 <?php if (isActive('admin/workcategory')) echo 'bg-blue-500/20' ?> hover:bg-blue-500/30 dark:text-white dark:opacity-80 text-sm ease-nav-brand my-0 mx-2 flex items-center whitespace-nowrap rounded-lg px-4 font-semibold text-slate-700 transition-colors">
                            <div class="mr-2 flex h-8 w-8 items-center justify-center rounded-lg bg-center stroke-0 text-center xl:p-2.5">
                                <i class="text-amber-500 fa fa-tasks"></i>
                            </div>
                            <span class="ml-1 duration-300 opacity-100 pointer-events-none ease">Work Categories</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="mt-0.5 w-full">
                <a href="<?= site_url('admin/post') ?>" class="py-2.7 <?php if (isActive('admin/post')) echo 'bg-blue-500/20' ?> hover:bg-blue-500/30 dark:text-white dark:opacity-80 text-sm ease-nav-brand my-0 mx-2 flex items-center whitespace-nowrap rounded-lg px-4 font-semibold text-slate-700 transition-colors" href="<?= site_url('admin/dashboard') ?>">
                    <div class="mr-2 flex h-8 w-8 items-center justify-center rounded-lg bg-center stroke-0 text-center xl:p-2.5">
                        <i class="text-lime-500 fa fa-send"></i>
                    </div>
                    <span class="ml-1 duration-300 opacity-100 pointer-events-none ease">Posts</span>
                </a>
            </li>
            <li class="mt-0.5 w-full">
                <a href="<?= site_url('admin/work') ?>" class="py-2.7 <?php if (isActive('admin/work')) echo 'bg-blue-500/20' ?> hover:bg-blue-500/30 dark:text-white dark:opacity-80 text-sm ease-nav-brand my-0 mx-2 flex items-center whitespace-nowrap rounded-lg px-4 font-semibold text-slate-700 transition-colors" href="<?= site_url('admin/dashboard') ?>">
                    <div class="mr-2 flex h-8 w-8 items-center justify-center rounded-lg bg-center stroke-0 text-center xl:p-2.5">
                        <i class="text-orange-500 fa fa-copy"></i>
                    </div>
                    <span class="ml-1 duration-300 opacity-100 pointer-events-none ease">Works</span>
                </a>
            </li>
            <li class="mt-0.5 w-full">
                <a href="<?= site_url('admin/profile') ?>" class="py-2.7 <?php if (isActive('admin/profile')) echo 'bg-blue-500/20' ?> hover:bg-blue-500/30 dark:text-white dark:opacity-80 text-sm ease-nav-brand my-0 mx-2 flex items-center whitespace-nowrap rounded-lg px-4 font-semibold text-slate-700 transition-colors" href="<?= site_url('admin/dashboard') ?>">
                    <div class="mr-2 flex h-8 w-8 items-center justify-center rounded-lg bg-center stroke-0 text-center xl:p-2.5">
                        <i class="text-violet-500 fa fa-id-card"></i>
                    </div>
                    <span class="ml-1 duration-300 opacity-100 pointer-events-none ease">Profile</span>
                </a>
            </li>
        </ul>
    </div>
</aside>
<!-- end sidenav -->