<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Portofolio | Posts</title>

	<link href="/assets/css/argon-dashboard-tailwind.css" rel="stylesheet" />
    <link rel="stylesheet" href="/assets/fonts/fontawesome/css/font-awesome.min.css">
	

	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;700&family=Roboto:wght@100&display=swap" rel="stylesheet">
</head>

<body>
<header class="flex w-full justify-end p-2.5">
		<div class="w-fit lg:hidden">
			<svg class="fill-we_current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" width="30px" height="30px">
				<path d="M 3 7 A 1.0001 1.0001 0 1 0 3 9 L 27 9 A 1.0001 1.0001 0 1 0 27 7 L 3 7 z M 3 14 A 1.0001 1.0001 0 1 0 3 16 L 27 16 A 1.0001 1.0001 0 1 0 27 14 L 3 14 z M 3 21 A 1.0001 1.0001 0 1 0 3 23 L 27 23 A 1.0001 1.0001 0 1 0 27 21 L 3 21 z" />
			</svg>
		</div>
		<div class="hidden flex-row justify-center items-center	 gap-8 py-3 px-16 text-xl capitalize lg:flex">
			<a href="<?= site_url() ?>#works">works</a>
			<a href="<?= site_url('work') ?>">blog</a>
			<a href="#contact">contact</a>
			<?php if ($this->session->has_userdata('user_id')): ?>
			<a class="px-6 py-1.5 rounded-md bg-we_red hover:bg-black hover:shadow-lg hover:shadow-we_red text-white hover:-translate-y-1" href="<?= site_url('dashboard') ?>">dashboard</a>
			<?php else: ?>
			<a class="px-6 py-1.5 rounded-md bg-we_red hover:bg-black hover:shadow-lg hover:shadow-we_red text-white hover:-translate-y-1" href="<?= site_url('auth/login') ?>">login</a>
			<?php endif; ?>
		</div>
	</header>
	<section id="blog" class="bg-we_blueSky text-we_current">
		<div class="container mb-4">
			<div class="mx-1 grid lg:grid-cols-2 justify-items-center gap-4 pb-9">
				<?php foreach($posts as $post):?>
				<div onclick="showWork(<?= $post['id'] ?>)" class="grid w-full gap-3 rounded-sm bg-white p-5 lg:p-6 hover:scale-105 hover:-translate-y-1 hover:drop-shadow hover:shadow cursor-pointer">
					<h2 class="text-xl lg:text-2xl font-bold capitalize"><?= $post['work'] ?></h2>
					<div class="my-1 flex flex-row">
						<h3 class="border-r-2 pr-6 lg:text-lg"><?= $post['year'] ?></h3>
                        <?php foreach($post['categories'] as $category): ?>
						<h3 class="px-6 lg:text-lg uppercase"><?= $category['name'] ?></h3>
                        <?php endforeach; ?>
					</div>
					<p class="line-clamp-3">
					<?= $post['description'] ?>
					</p>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
	<footer id="contact" class="mt-36">
		<div class="container">
			<div class="mx-14 my-14 md:mx-56 lg:mx-60">
				<div class="flex flex-row items-center justify-between">
					<svg class="fill-we_current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" width="40px" height="40px">
						<path d="M48,7H16c-4.418,0-8,3.582-8,8v32c0,4.418,3.582,8,8,8h17V38h-6v-7h6v-5c0-7,4-11,10-11c3.133,0,5,1,5,1v6h-4 c-2.86,0-4,2.093-4,4v5h7l-1,7h-6v17h8c4.418,0,8-3.582,8-8V15C56,10.582,52.418,7,48,7z" />
					</svg>
					<svg class="fill-we_current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="40px" height="40px">
						<path d="M 16.5 5 C 10.16639 5 5 10.16639 5 16.5 L 5 31.5 C 5 37.832757 10.166209 43 16.5 43 L 31.5 43 C 37.832938 43 43 37.832938 43 31.5 L 43 16.5 C 43 10.166209 37.832757 5 31.5 5 L 16.5 5 z M 16.5 8 L 31.5 8 C 36.211243 8 40 11.787791 40 16.5 L 40 31.5 C 40 36.211062 36.211062 40 31.5 40 L 16.5 40 C 11.787791 40 8 36.211243 8 31.5 L 8 16.5 C 8 11.78761 11.78761 8 16.5 8 z M 34 12 C 32.895 12 32 12.895 32 14 C 32 15.105 32.895 16 34 16 C 35.105 16 36 15.105 36 14 C 36 12.895 35.105 12 34 12 z M 24 14 C 18.495178 14 14 18.495178 14 24 C 14 29.504822 18.495178 34 24 34 C 29.504822 34 34 29.504822 34 24 C 34 18.495178 29.504822 14 24 14 z M 24 17 C 27.883178 17 31 20.116822 31 24 C 31 27.883178 27.883178 31 24 31 C 20.116822 31 17 27.883178 17 24 C 17 20.116822 20.116822 17 24 17 z" />
					</svg>
					<svg class="fill-we_current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" width="36px" height="36px">
						<path d="M 50.0625 10.4375 C 48.214844 11.257813 46.234375 11.808594 44.152344 12.058594 C 46.277344 10.785156 47.910156 8.769531 48.675781 6.371094 C 46.691406 7.546875 44.484375 8.402344 42.144531 8.863281 C 40.269531 6.863281 37.597656 5.617188 34.640625 5.617188 C 28.960938 5.617188 24.355469 10.21875 24.355469 15.898438 C 24.355469 16.703125 24.449219 17.488281 24.625 18.242188 C 16.078125 17.8125 8.503906 13.71875 3.429688 7.496094 C 2.542969 9.019531 2.039063 10.785156 2.039063 12.667969 C 2.039063 16.234375 3.851563 19.382813 6.613281 21.230469 C 4.925781 21.175781 3.339844 20.710938 1.953125 19.941406 C 1.953125 19.984375 1.953125 20.027344 1.953125 20.070313 C 1.953125 25.054688 5.5 29.207031 10.199219 30.15625 C 9.339844 30.390625 8.429688 30.515625 7.492188 30.515625 C 6.828125 30.515625 6.183594 30.453125 5.554688 30.328125 C 6.867188 34.410156 10.664063 37.390625 15.160156 37.472656 C 11.644531 40.230469 7.210938 41.871094 2.390625 41.871094 C 1.558594 41.871094 0.742188 41.824219 -0.0585938 41.726563 C 4.488281 44.648438 9.894531 46.347656 15.703125 46.347656 C 34.617188 46.347656 44.960938 30.679688 44.960938 17.09375 C 44.960938 16.648438 44.949219 16.199219 44.933594 15.761719 C 46.941406 14.3125 48.683594 12.5 50.0625 10.4375 Z" />
					</svg>
					<svg class="fill-we_current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="34px" height="34px">
						<path d="M 21.800781 0 L 2.199219 0 C 1 0 0 1 0 2.199219 L 0 21.800781 C 0 23 1 24 2.199219 24 L 21.800781 24 C 23 24 24 23 24 21.800781 L 24 2.199219 C 24 1 23 0 21.800781 0 Z M 7 20 L 3 20 L 3 9 L 7 9 Z M 5 7.699219 C 3.800781 7.699219 3 6.898438 3 5.898438 C 3 4.800781 3.800781 4 5 4 C 6.199219 4 7 4.800781 7 5.800781 C 7 6.898438 6.199219 7.699219 5 7.699219 Z M 21 20 L 17 20 L 17 14 C 17 12.398438 15.898438 12 15.601563 12 C 15.300781 12 14 12.199219 14 14 C 14 14.199219 14 20 14 20 L 10 20 L 10 9 L 14 9 L 14 10.601563 C 14.601563 9.699219 15.601563 9 17.5 9 C 19.398438 9 21 10.5 21 14 Z" />
					</svg>
				</div>
				<h3 class="mt-6 text-center text-sm">Copyright ©2020 All rights reserved</h3>
			</div>
		</div>
	</footer>

    <!-- Modal -->
	<div id="modal-item" class="hidden opacity-0 fixed h-screen w-screen inset-0 z-990 bg-black/50 overflow-x-hidden overflow-y-auto transition-opacity ease-linear outline-0">
        <div class="h-full w-full flex items-center justify-center">
            <div class="bg-white rounded-lg w-[44em] p-3">
                <div class="w-full">
                    <button id="modal_close" title="close modal" class="float-right hover:text-red-500">
                        <i class="fa-lg fa fa-times"></i>
                    </button>
                </div>
                <form id="modal_form" form-target="" action="">
                    <div class="grid gap-6 justify-items-center py-6 px-8">
                        <h2 id="modal_title" class="text-2xl capitalize"></h2>
                        <div id="modal_container" class="flex flex-col gap-y-2 gap-x-5 w-full justify-center">
                        </div>
                        <div class="">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Modal -->
</body> 


<script src="/assets/js/jquery.min.js"></script>

<script>
const site = '<?= site_url() ?>'

$('#modal_close').click(() => {
	toggleModal();
})

function toggleModal() {
	$('#modal-item').toggleClass('hidden');
	$('#modal-item').toggleClass('opacity-0');
}

function showWork(id) {
	$.get(site+'work/get/'+id)
		.done(response => {
			work = response.data
			$('#modal_title').text(work.work)
			$('#modal_container').html(`
				<div>
					<span class="capitalize">categories: </span>
					<span class="capitalize">${work.categories}</span>
				</div>
				<div>
					<span class="capitalize">work: </span>
					<span class="capitalize">${work.work}</span>
				</div>
				<div>
					<span class="capitalize">year: </span>
					<span class="capitalize">${work.year}</span>
				</div>
				<div>
					<span class="capitalize">city: </span>
					<span class="capitalize">${work.city}</span>
				</div>
				<div>
					<span class="capitalize">position: </span>
					<span class="capitalize">${work.position}</span>
				</div>
				<div>
					<span class="capitalize">description: </span>
					<p class="m-0 p-0">${work.description}</p>
				</div>
			`)
			toggleModal()
		})
}
</script>
</html>

