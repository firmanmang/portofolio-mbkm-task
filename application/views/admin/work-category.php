<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <title>Work Work</title>
    <!--     Fonts and icons     -->
    <!---    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />    --->
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="/assets/fonts/fontawesome/css/font-awesome.min.css">
    <!-- Nucleo Icons -->
    <link href="/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Popper -->
    <!-- <script src="https://unpkg.com/@popperjs/core@2"></script> -->
    <!-- Main Styling -->
    <link href="/assets/css/argon-dashboard-tailwind.css" rel="stylesheet" />
    
    <script src="/assets/js/jquery-1.min.js"></script>
    <script src="/assets/js/swal2.min.js"></script>
    
    <!-- <script src="https://code.jquery.com/jquery-3.6.1.slim.js" integrity="sha256-tXm+sa1uzsbFnbXt8GJqsgi2Tw+m4BLGDof6eUPjbtk=" crossorigin="anonymous"></script> -->
    <script src="/assets/js/datatables.min.js"></script>
    <!-- <link rel="stylesheet" href="/assets/css/datatables.min.css"> -->
</head>

<body class="m-0 font-sans text-base antialiased font-normal dark:bg-slate-900 leading-default bg-gray-50 text-slate-500">
    <div class="absolute w-full bg-blue-500 dark:hidden min-h-75"></div>
    <?php component('sidenav') ?>

    <main class="relative h-full max-h-screen transition-all duration-200 ease-in-out xl:ml-68 rounded-xl">

        <?php component('navbar', ['title' => 'work category']) ?>
        <!-- cards -->
        <div class="min-h-screen w-full px-6 py-6 mx-auto">
            <div class="flex flex-wrap -mx-3">
                <div class="flex-none w-full max-w-full px-3">
                    <div class="relative flex flex-col min-w-0 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
                        <!-- Heading -->
                        <div class="">
                            <div class="flex flex-col justify-center w-fit">
                                <div class="p-6 pb-0 mb-0 border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
                                    <h6 class="dark:text-white">Work Category table</h6>
                                </div>
                                <div class=" px-6 ">
                                    <button id="modal-trigger" class="btn btn-primary"><i class="fa fa-plus mx-2"></i> Add</button>
                                </div>
                            </div>
                        </div>
                        <!-- Heading -->

                        <!-- Tabel Content -->
                        <div class="flex-auto px-5 pt-0 pb-2">
                            <div class="p-4 overflow-x-auto">
                                <table id="table_work_category" class="mx-auto dark:text-white bg-gray-200 dark:bg-slate-800 col-span-2 p-4 rounded-2xl items-center w-full mb-0 align-top border-collapse dark:border-white/40 text-slate-500">
                                    <thead class="align-bottom">
                                        <tr>
                                            <th class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40 dark:text-white tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                Category Name
                                            </th>
                                            <th class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40 dark:text-white tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                Works
                                            </th>
                                            <th class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40 dark:text-white tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                Action
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>                                      
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- End Tabel Content -->

                    </div>
                </div>
            </div>

            <?php component('footer') ?>
        </div>
        <!-- end cards -->
    </main>

    <?php component('right-conf') ?>

    <div id="modal-item" class="hidden opacity-0 sticky h-screen w-screen inset-0 z-990 bg-black/50 overflow-x-hidden overflow-y-auto transition-opacity ease-linear outline-0">
        <div class="h-full w-full flex items-center justify-center">
            <div class="bg-white rounded-lg w-[32em] p-3">
                <div class="w-full">
                    <button id="modal-close" title="close modal" class="float-right hover:text-red-500">
                        <i class="fa-lg fa fa-times"></i>
                    </button>
                </div>
                <form id="category-form" action="">
                    <div class="grid gap-6 justify-items-center py-6 px-8">
                        <h2 id="model-title" class="text-2xl">Add new Work</h2>
                        <div class="flex flex-col md:flex-row gap-y-2 gap-x-5 w-full justify-center">
                            <label for="name">Work Name</label>
                            <div class="w-3/5">
                                <input id="name" type="text" name="name" class="w-full focus:shadow-primary-outline text-sm leading-5.6 ease block appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 outline-none transition-all placeholder:text-gray-500 focus:border-blue-500 focus:outline-none">
                                <p id="name_error" class="text-sm text-red-500"></p>
                            </div>
                        </div>
                        <div class="">
                            <button id="modal_execute_btn" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script src="/assets/js/main.js"></script>
    <script>
        var url = '<?= site_url('admin/workcategory') ?>';
        var action = 'store';
        var categoryInput = $('#name');
        var selectedWork = null;
        var inputs = {
            name: $('name'),
        }       
        
        let dataHelper = new DataHelper();
        var categories;
        let dataTable = $('#table_work_category').DataTable({
            ajax: getUrl('get-all'),
            order: [[0, 'asc']],
            pageLength: 50,
            columnDefs: [
                {
                    data: 'name',
                    targets: 0,
                },
                {
                    data: 'works',
                    targets: 1,
                    render: works => {
                        var ulWrapper = $('<ul>');
                        works.forEach(work => {
                            ulWrapper.append($('<li>', {text: work.work}))
                        })

                        return ulWrapper.html();
                    }
                },
                {
                    targets: 2,
                    data: 'id',
                    render: function (id, type, set, meta ) {
                        return `
                        <button onclick="edit(${id})" class="btn btn-sm btn-warning mx-1"> 
                            <span class="fa fa-pencil mr-1"></span>
                            Edit
                           </button>`;
                    }
                }
            ]
        });

        function refreshDataTable() {
            dataTable.ajax.reload();
        }
        
        
        function getUrl(path = '') {
            return url+'/'+path
        }

        $('#modal-trigger, #modal-close').click(function() {
            action = 'store';
            resetInputModal();
            toggleModal();
        });

        $('#modal_execute_btn').click(function(e) {
            e.preventDefault();
            if (action == 'store') {
                store();
            } else if (action == 'edit') {
                selectedWork.name = categoryInput.val();
                update();
            }
        });

        
        function resetInputModal() {
            categoryInput.val('');
        }

        function toggleModal() {
            resetErrorMessage(inputs);
            $('#modal-item').toggleClass('hidden');
            $('#modal-item').toggleClass('opacity-0');
        }

        async function get(id) {
            await dataHelper
                .get(getUrl('show/'+id))
                .then(function(result) {
                    selectedWork = result;
                });
        }
        
        function edit(id) {
            action = 'edit';
            get(id).then(function() {
                categoryInput.val(selectedWork.name)});
            toggleModal();

        }

        function store() {
            let data = $('#category-form').serialize();
            let successCallback = function(result) {
                    if (result.status == 'validation-error') {
                        for(key in result.errors) {
                            $('#' + key + '_error').html(result.errors[key]);
                        }
                    } else {
                        Toast.fire({
                            icon: result.status,
                            title: result.message,
                        })
                    }
                    
                    if (result.status == 'success') {
                        toggleModal();
                        refreshDataTable();
                    }
                }
                
            dataHelper.store(getUrl('store'), data, successCallback);
        }

        function update(activeStatus = null) {
            let workUpdate = {
                name: selectedWork.name,
                is_active: (activeStatus == null) 
                    ? selectedWork.is_active 
                    : activeStatus,
            }

            let successCallback = function(data) {
                    if (data.status == 'validation-error') {
                        for(key in data.errors) {
                            $('#' + key + '_error').html(data.errors[key]);
                        }
                    } else {
                        Toast.fire({
                            icon: data.status,
                            title: data.message,
                        })
                    }

                    if (data.status == 'success') {
                        $('#modal-item').addClass('hidden');
                        $('#modal-item').addClass('opacity-0');
                        refreshDataTable();
                    }
                }
            
            dataHelper.update(getUrl('update/'+selectedWork.id), 
                workUpdate, successCallback)
        }

    </script>

</body>
<!-- plugin for charts  -->
<script src="/assets/js/plugins/chartjs.min.js"></script>
<!-- plugin for scrollbar  -->
<script src="/assets/js/plugins/perfect-scrollbar.min.js"></script>
<!-- main script file  -->
<script src="/assets/js/argon-dashboard-tailwind.js?v=1.0.1"></script>

</html>