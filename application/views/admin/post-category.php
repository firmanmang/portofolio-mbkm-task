<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <title>Post Category</title>
    <!--     Fonts and icons     -->
    <!---    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />    --->
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="/assets/fonts/fontawesome/css/font-awesome.min.css">
    <!-- Nucleo Icons -->
    <link href="/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Popper -->
    <!-- <script src="https://unpkg.com/@popperjs/core@2"></script> -->
    <!-- Main Styling -->
    <link href="/assets/css/argon-dashboard-tailwind.css" rel="stylesheet" />
    
    <script src="/assets/js/jquery-1.min.js"></script>
    <script src="/assets/js/swal2.min.js"></script>
    
    <!-- <script src="https://code.jquery.com/jquery-3.6.1.slim.js" integrity="sha256-tXm+sa1uzsbFnbXt8GJqsgi2Tw+m4BLGDof6eUPjbtk=" crossorigin="anonymous"></script> -->
    <script src="/assets/js/datatables.min.js"></script>
    <!-- <link rel="stylesheet" href="/assets/css/datatables.min.css"> -->
</head>

<body class="m-0 font-sans text-base antialiased font-normal dark:bg-slate-900 leading-default bg-gray-50 text-slate-500">
    <div class="absolute w-full bg-blue-500 dark:hidden min-h-75"></div>
    <?php component('sidenav') ?>

    <main class="relative h-full max-h-screen transition-all duration-200 ease-in-out xl:ml-68 rounded-xl">

        <?php component('navbar', ['title' => 'post category']) ?>
        <!-- cards -->
        <div class="min-h-screen w-full px-6 py-6 mx-auto">
            <div class="flex flex-wrap -mx-3">
                <div class="flex-none w-full max-w-full px-3">
                    <div class="relative flex flex-col min-w-0 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
                        <!-- Heading -->
                        <div class="">
                            <div class="flex flex-col justify-center w-fit">
                                <div class="p-6 pb-0 mb-0 border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
                                    <h6 class="dark:text-white">Post Category table</h6>
                                </div>
                                <div class=" px-6 ">
                                    <button id="modal-trigger" class="btn btn-primary"><i class="fa fa-plus mx-2"></i> Add</button>
                                </div>
                            </div>

                            <!-- <div class="p-6 flex gap-2 flex-row items-center w-fit transition-all rounded-lg ease">
                                <div class="w-full">
                                    <span class="text-sm ease leading-5.6 absolute z-50 -ml-px flex h-fit items-center whitespace-nowrap rounded-lg rounded-tr-none rounded-br-none border border-r-0 border-transparent bg-transparent py-2 px-2.5 text-center font-normal text-slate-500 transition-all">
                                        <i class="fa fa-search"></i>
                                    </span>
                                    <input type="text" class="pl-9 text-sm focus:shadow-primary-outline ease w-full leading-5.6 relative -ml-px block min-w-0 flex-auto rounded-lg border border-solid border-gray-300 dark:bg-slate-850 dark:text-white bg-white bg-clip-padding py-2 pr-3 text-gray-700 transition-all placeholder:text-gray-500 focus:border-blue-500 focus:outline-none focus:transition-shadow" placeholder="Type here..." />
                                </div>
                                <button class="btn btn-outline-primary" type="submit">Search</button>
                            </div> -->
                        </div>
                        <!-- Heading -->

                        <!-- Tabel Content -->
                        <div class="flex-auto px-5 pt-0 pb-2">
                            <div class="p-4 overflow-x-auto">
                                <table id="table_post_category" class="mx-auto dark:text-white bg-gray-200 dark:bg-slate-800 col-span-2 p-4 rounded-2xl items-center w-full mb-0 align-top border-collapse dark:border-white/40 text-slate-500">
                                    <thead class="align-bottom">
                                        <tr>
                                            <th class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40 dark:text-white tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                #
                                            </th>
                                            <th class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40 dark:text-white tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                Category Name
                                            </th>
                                            <th class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40 dark:text-white tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                Status
                                            </th>
                                            <th class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40 dark:text-white tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                Action
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>                                      
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- End Tabel Content -->

                    </div>
                </div>
            </div>

            <?php component('footer') ?>
        </div>
        <!-- end cards -->
    </main>

    <?php component('right-conf') ?>

    <div id="modal-item" class="hidden opacity-0 sticky h-screen w-screen inset-0 z-990 bg-black/50 overflow-x-hidden overflow-y-auto transition-opacity ease-linear outline-0">
        <div class="h-full w-full flex items-center justify-center">
            <div class="bg-white rounded-lg w-[32em] p-3">
                <div class="w-full">
                    <button id="modal-close" title="close modal" class="float-right hover:text-red-500">
                        <i class="fa-lg fa fa-times"></i>
                    </button>
                </div>
                <form id="category-form" action="">
                    <div class="grid gap-6 justify-items-center py-6 px-8">
                        <h2 id="model-title" class="text-2xl">Add new Category</h2>
                        <div class="flex flex-col md:flex-row gap-y-2 gap-x-5 w-full justify-center">
                            <label for="category_name">Category Name</label>
                            <div class="w-3/5">
                                <input id="category_name" type="text" name="category_name" class="w-full focus:shadow-primary-outline text-sm leading-5.6 ease block appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 outline-none transition-all placeholder:text-gray-500 focus:border-blue-500 focus:outline-none">
                                <p id="category_name_error" class="text-sm text-red-500"></p>
                            </div>
                        </div>
                        <div class="">
                            <button id="modal_execute_btn" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script src="/assets/js/main.js"></script>
    <script>
        var url = '<?= site_url('admin/postcategory') ?>';
        var action = 'store';
        var categoryInput = $('#category_name');
        var selectedCategory = null;
        var inputs = {
            category_name: $('category_name'),
        }       
        
        let dataHelper = new DataHelper();
        var categories;
        let dataTable = $('#table_post_category').DataTable({
            ajax: getUrl('datatable'),
            serverSide: true,
            // order: [[0, 'asc']],
            pageLength: 50,
            columnDefs: [
                {
                    data: 'id',
                    targets: 0,
                    name: 'id',
                },
                {
                    data: 'category_name',
                    name: 'category_name',
                    targets: 1,
                },
                {
                    data: 'is_active',
                    name: 'is_active',
                    visible: false,
                    searchable: false,
                },
                {
                    targets: 2,
                    data: 'is_active',
                    name: 'is_active',
                    orderData: 2,
                    searchable: false,
                    render: function (isActive, type, set, meta ) {
                        return (isActive == 1)
                            ? `<span class="bg-gradient-to-tl from-emerald-500 to-teal-400 px-2.5 text-xs rounded-1.8 py-1.4 inline-block whitespace-nowrap text-center align-baseline font-bold uppercase leading-none text-white">
                                    Active
                                </span>`
                            : `<span class="bg-gradient-to-tl from-slate-600 to-slate-300 px-2.5 text-xs rounded-1.8 py-1.4 inline-block whitespace-nowrap text-center align-baseline font-bold uppercase leading-none text-white">
                                    Inactive
                                </span>`;
                    }
                    
                },
                {
                    targets: 3,
                    data: 'id',
                    name: 'id',
                    searchable: false,
                    render: function (id, type, set, meta ) {
                        return `
                        <button onclick="edit(${id})" class="btn btn-sm btn-warning mx-1"> 
                            <span class="fa fa-pencil mr-1"></span>
                            Edit
                        </button>` +
                        ((set.is_active == 1)
                            ? `<button onclick="disableStatus(${id})" class="btn btn-sm btn-danger mx-1"> 
                                    <span class="fa fa-times mr-1"></span>
                                    Disable
                                </button>`
                            : `<button onclick="enableStatus(${id})" class="btn btn-sm btn-success mx-1"> 
                                <span class="fa fa-check mr-1"></span>
                                Enable
                            </button>`);
                }
            }]
        });

        function refreshDataTable() {
            dataTable.ajax.reload();
        }
        
        
        function getUrl(path = '') {
            return url+'/'+path
        }

        $('#modal-trigger, #modal-close').click(function() {
            action = 'store';
            resetInputModal();
            toggleModal();
        });

        $('#modal_execute_btn').click(function(e) {
            e.preventDefault();
            if (action == 'store') {
                store();
            } else if (action == 'edit') {
                selectedCategory.category_name = categoryInput.val();
                update();
            }
        });

        
        function resetInputModal() {
            categoryInput.val('');
        }

        function toggleModal() {
            resetErrorMessage(inputs);
            $('#modal-item').toggleClass('hidden');
            $('#modal-item').toggleClass('opacity-0');
        }

        async function get(id) {
            await dataHelper
                .get(getUrl('show/'+id))
                .then(function(result) {
                    selectedCategory = result;
                });
        }
        
        function edit(id) {
            action = 'edit';
            get(id).then(function() {
                categoryInput.val(selectedCategory.category_name)});
            toggleModal();

        }

        function disableStatus(id) {
            get(id).then(function() {
                update(0);
                refreshDataTable();
            });
        }

        function enableStatus(id) {
            get(id).then(function() {
                update(1);
                refreshDataTable();
            });
        }

        function store() {
            let data = $('#category-form').serialize();
            let successCallback = function(result) {
                    if (result.status == 'validation-error') {
                        for(key in result.errors) {
                            $('#' + key + '_error').html(result.errors[key]);
                        }
                    } else {
                        Toast.fire({
                            icon: result.status,
                            title: result.message,
                        })
                    }
                    
                    if (result.status == 'success') {
                        toggleModal();
                        refreshDataTable();
                    }
                }
                
            dataHelper.store(getUrl('store'), data, successCallback);
        }

        function update(activeStatus = null) {
            let cetegoryUpdate = {
                category_name: selectedCategory.category_name,
                is_active: (activeStatus == null) 
                    ? selectedCategory.is_active 
                    : activeStatus,
            }

            let successCallback = function(data) {
                    if (data.status == 'validation-error') {
                        for(key in data.errors) {
                            $('#' + key + '_error').html(data.errors[key]);
                        }
                    } else {
                        Toast.fire({
                            icon: data.status,
                            title: data.message,
                        })
                    }

                    if (data.status == 'success') {
                        $('#modal-item').addClass('hidden');
                        $('#modal-item').addClass('opacity-0');
                        refreshDataTable();
                    }
                }
            
            dataHelper.update(getUrl('update/'+selectedCategory.id), 
                cetegoryUpdate, successCallback)
        }

    </script>

</body>
<!-- plugin for charts  -->
<script src="/assets/js/plugins/chartjs.min.js"></script>
<!-- plugin for scrollbar  -->
<script src="/assets/js/plugins/perfect-scrollbar.min.js"></script>
<!-- main script file  -->
<script src="/assets/js/argon-dashboard-tailwind.js?v=1.0.1"></script>

</html>