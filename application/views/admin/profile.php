<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <title>Title</title>
    <!--     Fonts and icons     -->
    <!---    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />    --->
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="/assets/fonts/fontawesome/css/font-awesome.min.css">
    <!-- Nucleo Icons -->
    <link href="/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Popper -->
    <!-- <script src="https://unpkg.com/@popperjs/core@2"></script> -->
    <!-- Main Styling -->
    <link href="/assets/css/argon-dashboard-tailwind.css" rel="stylesheet" />
    <link rel="stylesheet" href="/assets/css/select2.min.css">
</head>

<body class="m-0 font-sans text-base antialiased font-normal dark:bg-slate-900 leading-default bg-gray-50 text-slate-500">
    <div class="absolute w-full bg-blue-500 dark:hidden min-h-75"></div>
    <?php component('sidenav') ?>

    <main class="relative h-full max-h-screen transition-all duration-200 ease-in-out xl:ml-68 rounded-xl">

        <?php component('navbar', ['title' => 'profile']) ?>

        <!-- Section -->

        <div class="min-h-screen w-full px-6 py-6 mx-auto">
            <div class="p-6 bg-white rounded-2xl">
                <!-- Tab -->
                <div class="mb-4 border-b border-gray-200 dark:border-gray-700">
                    <ul class="flex flex-wrap -mb-px text-sm font-medium text-center" id="myTab" data-tabs-toggle="#myTabContent" role="tablist">
                        <li class="mr-2" role="presentation">
                            <button id="general-tab" class="tab inline-block p-4 rounded-t-lg border-b-2 capitalize" data-tabs-target="#general" aria-selected="true">general</button>
                        </li>
                        <li class="mr-2" role="presentation">
                            <button id="experience-tab" class="tab inline-block p-4 rounded-t-lg border-b-2 capitalize" data-tabs-target="#table_section" aria-selected="false">experience</button>
                        </li>
                        <li class="mr-2" role="presentation">
                            <button id="education-tab" class="tab inline-block p-4 rounded-t-lg border-b-2 capitalize" data-tabs-target="#table_section" aria-selected="false">education</button>
                        </li>
                        <li class="mr-2" role="presentation">
                            <button id="language-tab" class="tab inline-block p-4 rounded-t-lg border-b-2 capitalize" data-tabs-target="#table_section" aria-selected="false">language</button>
                        </li>
                        <li class="mr-2" role="presentation">
                            <button id="hobby-tab" class="tab inline-block p-4 rounded-t-lg border-b-2 capitalize" data-tabs-target="#table_section" aria-selected="false">hobby</button>
                        </li>
                        <li class="mr-2" role="presentation">
                            <button id="skill-tab" class="tab inline-block p-4 rounded-t-lg border-b-2 capitalize" data-tabs-target="#table_section" aria-selected="false">skill</button>
                        </li>
                        <li class="mr-2" role="presentation">
                            <button id="socialmedia-tab" class="tab inline-block p-4 rounded-t-lg border-b-2 capitalize" data-tabs-target="#table_section" aria-selected="false">socialmedia</button>
                        </li>
                        <li class="mr-2" role="presentation">
                            <button id="cv-tab" class="tab inline-block p-4 rounded-t-lg border-b-2 uppercase" data-tabs-target="#cv" aria-selected="false">cv</button>
                        </li>
                    </ul>
                </div>
                <!-- End Tab -->
                <!-- Tab Content -->
                <div class="">
                    <!-- General Section -->
                    <div id="general" class="content-tab flex flex-wrap -mx-3">
                        <!-- Update Password -->
                        <div class="w-full max-w-full px-3 mt-6 shrink-0 md:w-4/12 md:flex-0 md:mt-0">
                            <form id="change-password" action="">
                                <div class="relative flex flex-col min-w-0 break-words bg-white border-0 shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
                                    <div class="relative flex flex-col min-w-0 break-words bg-white border-0 shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
                                        <div class="border-black/12.5 rounded-t-2xl border-b-0 border-solid p-6 pb-0">
                                            <div class="flex items-center">
                                                <p class="mb-0 dark:text-white/80">Change passsword</p>
                                            </div>
                                        </div>
                                        <div class="flex-auto p-6">
                                            <div class="flex flex-col -mx-3">
                                                <div class="w-full max-w-full px-3 shrink-0">
                                                    <div class="mb-4">
                                                        <label for="current_password" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80">Current password</label>
                                                        <input type="password" name="current_password" class="input block w-full " />
                                                        <p id="current_password_error" class="text-sm text-red-500"></p>
                                                    </div>
                                                </div>
                                                <div class="w-full max-w-full px-3 shrink-0">
                                                    <div class="mb-4">
                                                        <label for="new_password" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80">New Password</label>
                                                        <input type="password" name="new_password" class="input block w-full " />
                                                        <p id="new_password_error" class="text-sm text-red-500"></p>
                                                    </div>
                                                </div>
                                                <div class="w-full max-w-full px-3 shrink-0">
                                                    <div class="mb-4">
                                                        <label for="retype_password" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80">Re-type New Password</label>
                                                        <input type="password" name="retype_password" class="input block w-full " />
                                                        <p id="retype_password_error" class="text-sm text-red-500"></p>
                                                    </div>
                                                </div>
                                                <div class="px-3">
                                                    <button type="submit" class="btn btn-warning">Change password</button>
                                                </div>
                                            </div>
                                            <hr class="hr-line" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- EndUpdate Password -->

                        <!-- Update Information -->
                        <div class="w-full max-w-full px-3 shrink-0 md:w-8/12 md:flex-0">
                            <form id="update-profile">
                                <div class="relative flex flex-col min-w-0 break-words bg-white border-0 shadow-3xl drop-shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
                                    <div class="border-black/12.5 rounded-t-2xl border-b-0 border-solid p-6 pb-0">
                                        <div class="flex items-center">
                                            <p class="mb-0 dark:text-white/80">Edit Profile</p>
                                        </div>
                                    </div>
                                    <div class="flex w-full items-center flex-col justify-center gap-3 p-6">
                                        <div class="w-48 px-3 flex-0">
                                            <a href="javascript:;">
                                                <img src="<?= ($user->image_profile == null) ? '/assets/img/avatar.jpg' : $user->image_profile ?>" id="preview-img" class="h-auto max-w-full border-2 border-gray-300 border-solid rounded-circle" alt="profile image">
                                            </a>
                                        </div>
                                        <label for="image_profile" class="btn btn-outline-primary">
                                            <span class="fa fa-upload mx-3"></span>
                                            Pick photo
                                        </label>
                                        <input class="hidden" type="file" name="image_profile" id="image_profile" accept="image/png, image/jpeg">
                                    </div>
                                    <div class="flex-auto p-6">
                                        <p class="leading-normal uppercase dark:text-white dark:opacity-60 text-sm">User Information</p>
                                        <div class="flex flex-wrap -mx-3">
                                            <div class="w-full max-w-full px-3 shrink-0 md:w-6/12 md:flex-0">
                                                <div class="mb-4">
                                                    <label for="name" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80">Name</label>
                                                    <input value="<?= $user->name ?>" type="text" name="name" id="name" class="input block w-full" />
                                                    <p id="name_error" class="text-sm text-red-500"></p>
                                                </div>
                                            </div>
                                            <div class="w-full max-w-full px-3 shrink-0 md:w-6/12 md:flex-0">
                                                <div class="mb-4">
                                                    <label for="username" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80">Username</label>
                                                    <input value="<?= $user->username ?>" type="text" name="username" id="username" class="input block w-full" />
                                                    <p id="username_error" class="text-sm text-red-500"></p>
                                                </div>
                                            </div>
                                            <div class="w-full max-w-full px-3 shrink-0 md:w-6/12 md:flex-0">
                                                <div class="mb-4">
                                                    <label for="email" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80">Email address</label>
                                                    <input value="<?= $user->email ?>" type="email" name="email" id="email" class="input block w-full" />
                                                    <p id="email_error" class="text-sm text-red-500"></p>
                                                </div>
                                            </div>
                                            <div class="w-full max-w-full px-3 shrink-0 md:w-6/12 md:flex-0">
                                                <div class="mb-4">
                                                    <label for="phone_number" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80">Phone Number</label>
                                                    <input value="<?= $user->phone_number ?>" type="text" name="phone_number" id="phone_number" class="input block w-full" />
                                                    <p id="phone_number_error" class="text-sm text-red-500"></p>
                                                </div>
                                            </div>
                                            <div class="w-full max-w-full px-3 shrink-0 md:w-6/12 md:flex-0">
                                                <div class="mb-4">
                                                    <label for="provinsi" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">provinsi</label>
                                                    <select name="provinsi_id" id="provinsi" class="p-2 w-full">
                                                        <option selected disabled>-- Pilih Provinsi --</option>
                                                        <?php foreach ($provinsiList as $provinsi) : ?>
                                                            <option value="<?= $provinsi->id ?>" class="capitalize">
                                                                <?= $provinsi->nama ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <p id="provinsi_error" class="text-sm text-red-500"></p>
                                                </div>
                                            </div>
                                            <div class="hidde md:block"></div>
                                            <div class="w-full max-w-full px-3 shrink-0 md:w-6/12 md:flex-0">
                                                <div class="mb-4">
                                                    <label for="kabupaten" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">kabupaten</label>
                                                    <select name="kabupaten_id" id="kabupaten" class="input block w-full"></select>
                                                    <p id="kabupaten_error" class="text-sm text-red-500"></p>
                                                </div>
                                            </div>
                                            <div class="hidde md:block"></div>
                                            <div class="w-full max-w-full px-3 shrink-0 md:w-6/12 md:flex-0">
                                                <div class="mb-4">
                                                    <label for="kecamatan" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">kecamatan</label>
                                                    <select name="kecamatan_id" id="kecamatan" class="input block w-full"></select>
                                                    <p id="kecamatan_error" class="text-sm text-red-500"></p>
                                                </div>
                                            </div>
                                            <div class="hidde md:block"></div>
                                            <div class="w-full max-w-full px-3 shrink-0 md:w-6/12 md:flex-0">
                                                <div class="mb-4">
                                                    <label for="desa" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">desa</label>
                                                    <select name="desa_id" id="desa" class="input block w-full"></select>
                                                    <p id="desa_error" class="text-sm text-red-500"></p>
                                                </div>
                                            </div>
                                            <div class="hidde md:block"></div>
                                            <div class="px-3">
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </div>
                                        </div>
                                        <hr class="hr-line" />
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- End Update Information -->
                    </div>
                    <!-- End General Section -->

                    <!-- Table Section -->
                    <div id="table_section" class="content-tab flex flex-wrap -mx-3">
                        <div class="flex-none w-full max-w-full px-3">
                            <div class="relative flex flex-col min-w-0 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
                                <!-- Heading Experience -->
                                <div class="">
                                    <div class="flex flex-col justify-center w-fit">
                                        <div class="p-6 pb-0 mb-0 border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
                                            <h6 id="table_title" class="dark:text-white capitalize"></h6>
                                        </div>
                                        <div class="flex gap-3 px-6 ">
                                            <button id="add_btn" class=" btn btn-primary"><i class="fa fa-plus mx-2"></i> Add</button>
                                            <button id="import_btn" class="btn btn-success"><i class="fa fa-upload mx-2"></i> Import Data</button>
                                            <button id="export_btn" class="btn btn-warning"><i class="fa fa-download mx-2"></i> Export Data</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Heading -->

                                <!-- Tabel -->
                                <div class="flex-auto px-5 pt-0 pb-2">
                                    <div class="p-4 overflow-x-auto">
                                        <table id="general_table" class="mx-auto dark:text-white bg-gray-200 dark:bg-slate-800 col-span-2 p-4 rounded-2xl items-center w-full mb-0 align-top border-collapse dark:border-white/40 text-slate-500">
                                            <thead id="thead_table" class="align-bottom">
                                            </thead>
                                            <tbody id="tbody_table">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- End Tabel -->

                            </div>
                        </div>
                    </div>
                    <!-- End Table Section -->

                    <!-- CV -->
                    <div id="cv" class="content-tab flex flex-wrap -mx-3">
                        <div class="flex-none w-full max-w-full px-3">
                            <div class="relative grid gap-1 min-w-0 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
                                <!-- Heading CV -->
                                <div class="p-6 w-full">
                                    <div class="flex flex-row justify-between items-center w-full">
                                        <div class="pb-0 mb-0 border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
                                            <h6 class="dark:text-white capitalize">Curriculum Vitae</h6>
                                        </div>
                                        <a href="<?= base_url('admin/cv/renderPdf') ?>" class="btn btn-primary h-fit">Download CV</a>
                                    </div>
                                </div>
                                <!-- End Heading CV -->

                                <div id="cv_section" class="mx-auto w-full md:w-4/6 lg:w-1/2 grid gap-8 rounded-lg border p-16 text-gray-700 shadow-lg">
                                    <div class="flex flex-row gap-24">
                                        <img class="w-2/6 h-auto rounded-full" src="<?= ($user->image_profile == null) ? '/assets/img/avatar.jpg' : $user->image_profile ?>" alt="image" />
                                        <div class="">
                                            <h1 id="cv_fullname" class="text-3xl font-bold capitalize">Your fullname</h1>
                                            <div class="">
                                                <h5 id="cv_address" class="text-gray-400 text-xs capitalize">Your address</h5>
                                                <h5 class="text-gray-400 text-xs">Email: <span id="cv_email">@email.com</span></h5>
                                                <h5 class="text-gray-400 text-xs">Phone: <span id="cv_phone">0812345678909</span></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="h-[2px] bg-gradient-to-r from-gray-200 via-gray-400 to-gray-200" />
                                    <!-- Experience -->
                                    <div class="">
                                        <h2 class="mb-3 text-xl capitalize">experience</h2>
                                        <div id="cv_experience" class="px-2 text-sm">
                                            <?php foreach($experiences as $experience): ?>
                                            <div class="flex gap-9">
                                                <span class="mr-4 block min-w-56 border-r-2 text-xs">
                                                    <?= $experience->start_date.' - '.$experience->resign_date ?>
                                                </span>
                                                <div class="">
                                                    <h4 class="mb-2 text-xl font-bold">
                                                        <?= $experience->name ?>
                                                    </h4>
                                                    <p class="text-xs text-gray-400">
                                                        <?= $experience->description ?>
                                                    </p>
                                                </div>
                                            </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <hr class="h-[2px] bg-gradient-to-r from-gray-200 via-gray-400 to-gray-200" />
                                    <!-- Education -->
                                    <div class="">
                                        <h2 class="mb-3 text-xl capitalize">education</h2>
                                        <div class="px-2 text-sm">
                                            <ul id="cv_education" class="grid list-disc">
                                                <?php foreach($educations as $education): ?>
                                                <li>
                                                    <div class="grid">
                                                        <p class="m-0 font-bold">
                                                            <?= $education->name ?>
                                                        </p>
                                                        <p class="m-0 text-xs">
                                                            <?= $education->start_date.' - '.$education->graduate_date ?>
                                                        </p>
                                                        <p class="m-0 text-xs">
                                                            <?= $education->description ?>
                                                        </p>
                                                    </div>
                                                </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <hr class="h-[2px] bg-gradient-to-r from-gray-200 via-gray-400 to-gray-200" />

                                    <div class="grid grid-cols-2 gap-8 text-xs">
                                        <!-- Hobby -->
                                        <div class="">
                                            <h2 class="mb-3 text-xl capitalize">Hobby</h2>
                                            <div class="px-2">
                                                <ul id="cv_hobby" class="grid gap-1 list-disc">
                                                <?php foreach($hobbies as $hobby): ?>
                                                    <li><?= $hobby->hobby ?></li>
                                                <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Language -->
                                        <div class="">
                                            <h2 class="mb-3 text-xl capitalize">language</h2>
                                            <div class="">
                                                <ul id="cv_language" class="grid gap-3">
                                                <?php foreach($languages as $language): ?>
                                                    <li>
                                                        <?= $language->language ?>
                                                            <div class="mt-1 w-56 rounded-full bg-gray-200">
                                                                <div style="width: <?= $language->level ?>%;" class="child rounded-full bg-gray-400 py-1"></div>
                                                            </div>
                                                    </li>
                                                <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Skill -->
                                        <div class="">
                                            <h2 class="mb-3 text-xl capitalize">Skill</h2>
                                            <div class="">
                                                <ul id="cv_skill" class="grid gap-3">
                                                <?php foreach($skills as $skill): ?>
                                                    <li>
                                                        <?= $skill->name ?>
                                                            <div class="mt-1 w-56 rounded-full bg-gray-200">
                                                                <div style="width: <?= $skill->level ?>%;" class="child rounded-full bg-gray-400 py-1"></div>
                                                            </div>
                                                    </li>
                                                <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Social Media -->
                                        <div class="">
                                            <h2 class="mb-3 text-xl capitalize">social media</h2>
                                            <div class="">
                                                <ul id="cv_socialmedia" class="grid gap-1">
                                                    <?php foreach($socialmedias as $socialmedia): ?>
                                                        <li><?= $socialmedia->name.' : '.$socialmedia->url ?></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="editor"></div>
                            </div>
                        </div>
                        <!-- End CV -->
                    </div>

                </div>

            </div>
            <!-- End Section -->

            
            <?php component('footer') ?>
        </div>
        <!-- end cards -->
    </main>

    <!-- Modal -->
    <div id="modal-item" class="hidden opacity-0 fixed h-screen w-screen inset-0 z-990 bg-black/50 overflow-x-hidden overflow-y-auto transition-opacity ease-linear outline-0">
        <div class="h-full w-full flex items-center justify-center">
            <div class="bg-white rounded-lg w-[32em] p-3">
                <div class="w-full">
                    <button id="modal_close" title="close modal" class="float-right hover:text-red-500">
                        <i class="fa-lg fa fa-times"></i>
                    </button>
                </div>
                <form id="modal_form" form-target="" action="">
                    <div class="grid gap-6 justify-items-center py-6 px-8">
                        <h2 id="model_title" class="text-2xl capitalize"></h2>
                        <div id="form_modal_container" class="flex flex-col gap-y-2 gap-x-5 w-full justify-center">
                        </div>
                        <div class="">
                            <button type="submit" id="modal_execute_btn" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Modal -->

    <div id="loading_section" class="hidden fixed inset-0 h-full bg-black/50 z-990 justify-center items-center overflow-hidden">
        <div class="grid text-center gap-3 text-white a">
            <i class="text-5xl fa fa-refresh animate-spin"></i>
            <div class="animate-pulse">
                Loading...
            </div>
        </div>
    </div>
    <?php component('right-conf') ?>
</body>
<!-- plugin for charts  -->
<script src="/assets/js/plugins/chartjs.min.js" async></script>
<!-- plugin for scrollbar  -->
<script src="/assets/js/plugins/perfect-scrollbar.min.js" async></script>
<!-- main script file  -->
<script src="/assets/js/argon-dashboard-tailwind.js?v=1.0.1" async></script>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/swal2.min.js"></script>
<script src="/assets/js/datatables.min.js"></script>
<script src="/assets/js/select2.min.js"></script>
<script src="/assets/js/tabs.js"></script>

<script>
    var message = '';
</script>
<script src="/assets/js/main.js"></script>
<script>
    var fullname = '<?= $user->name ?>'
    var email = '<?= $user->email ?>'
    var phone = '<?= $user->phone_number ?>'

    var resource = tabActive.attr('id').replace('-tab', '');
    var action = 'store'
    var activeId = null;
    var generalDataTable = null;
    const wilayah = '<?= site_url('wilayah') ?>';
    const siteUrl = '<?= site_url() ?>';
    const user = JSON.parse('<?= json_encode($user) ?>')
    const modalTitle = $('#model_title')
    const formModalContainer = $('#form_modal_container')
    const form = $('#modal_form');
    const modalInputs = {
        experience: {
            name: 'text',
            start_date: 'date',
            resign_date: 'date',
            description: 'textarea',
        },
        education: {
            name: 'text',
            start_date: 'date',
            graduate_date: 'date',
            description: 'textarea',
        },
        language: {
            language: 'text',
            level: 'range',
        },
        hobby: {
            hobby: 'text',
        },
        skill: {
            name: 'text',
            level: 'range',
        },
        socialmedia: {
            name: 'text',
            url: 'text',
        }
    }


    $('#table_title').text(resource);

    function setInputModal(inputs) {
        formModalContainer.html('');

        for (key in inputs) {
            let inputWrapper = $('<div>', {
                class: 'flex flex-col gap-1',
                id: key + '_input_wrapper_modal'
            })

            inputWrapper
                .append($('<label>', {
                    for: key + '_input_modal',
                    class: 'capitalize',
                    text: key.replace('_', ' '),
                }))

            if (inputs[key] == 'textarea') {
                inputWrapper
                    .append($('<textarea>', {
                        rows: 8,
                        class: 'input w-full',
                        id: key + '_input_modal',
                        name: key
                    }))
                    .append($('<p>', {
                        class: 'text-sm text-red-500',
                        id: key + '_error_modal',
                    }))
            } else {
                inputWrapper
                    .append($('<input>', {
                        name: key,
                        type: inputs[key],
                        id: key + '_input_modal',
                        class: 'input w-full',
                    }))
                    .append($('<p>', {
                        class: 'text-sm text-red-500',
                        id: key + '_error_modal',
                    }))
            }

            formModalContainer.append(inputWrapper)
        }
    }

    function toggleModal() {
        resetErrorMessageModal(modalInputs[resource]);
        $('#modal-item').toggleClass('hidden');
        $('#modal-item').toggleClass('opacity-0');
    }

    function resetErrorMessageModal(inputs) {
        for (key in inputs) {
            $('#' + key + '_error_modal').html('');
        }
    }

    function edit(id) {
        action = 'edit'
        activeId = id
        modalTitle.html('edit ' + resource)
        setInputModal(modalInputs[resource]);
        $.get(siteUrl + `admin/cv/get/${resource}/${id}`)
            .done(response => {
                var data = response.data
                toggleModal()
                for (key in modalInputs[resource]) {
                    $('#' + key + '_input_modal').val(data[key])
                }
            })
    }

    function destroy(id) {
        action = 'delete'
        activeId = id

        Swal.fire({
            title: 'Are you sure?',
            text: 'Do you want delete?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#60a5fa',
            cancelButtonColor: '#f53939',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.post(siteUrl + `admin/cv/delete/${resource}/${id}`)
                    .done(function() {
                        Toast.fire({
                            icon: 'success',
                            title: resource + ' has been deleted.'
                        })
                        refreshDatatable()
                    })
            }
        })

    }

    function setTable() {
        var theadTable = $('#thead_table');
        var tbodyTable = $('#tbody_table');
        var generalTable = $('#general_table')
        var columnList = []
        var trWrapper = $('<tr>')
        var columnsLabel = {
            ...modalInputs[resource]
        };

        columnsLabel['id'] = ''

        if (generalDataTable != null) {
            generalDataTable.clear()
            generalDataTable.destroy()
        }

        theadTable.empty()
        tbodyTable.empty()

        for (key in columnsLabel) {
            trWrapper.append($('<th>', {
                class: 'px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40 dark:text-white tracking-none whitespace-nowrap text-slate-400 opacity-70',
                text: key.replace('_', ' ')
            }))

            columnList.push({
                data: key,
                className: 'max-w-32 truncate'
            })
        }
        theadTable.append(trWrapper);

        generalDataTable = generalTable.DataTable({
            ajax: {
                url: siteUrl + 'admin/cv/get-all/' + resource,
                dataSrc: '',
                beforeSend: () => {
                    toggleLoading()
                },
                complete: () => {
                    toggleLoading()
                }
            },
            pageLength: 50,
            columns: [...columnList],
            columnDefs: [{
                    data: 'id',
                    title: 'action',
                    targets: -1,
                    className: 'w-full',
                    render: id => {
                        return `
                        <button onClick="edit(${id})" class="btn btn-sm btn-warning mx-1"> 
                            <span class="fa fa-pencil mr-1"></span>
                            Edit
                        </button>
                        <button onClick="destroy(${id})" class="btn btn-sm btn-danger mx-1"> 
                            <span class="fa fa-trash mr-1"></span>
                            Delete
                        </button>
                        `
                    }
                }

            ]
        })
    }

    function refreshDatatable() {
        if (generalDataTable != null) {
            generalDataTable.ajax.reload()
        }
    }

    function setImportModal() {
        formModalContainer.html('')
        formModalContainer.append(
            $('<div>', {
                class: 'grid gap-5',
            })
            .append(
                $('<div>', {
                    text: 'Please use file following the template',
                })
                .append(
                    $('<label>', {
                        class: 'btn btn-primary block',
                        text: 'Import '+ resource,
                        for: 'file_import'
                    }).append($('<i>', {
                        class: 'fa fa-upload mx-2',
                    }))
                )
                .append(
                    $('<input>', {
                        type: 'file',
                        id: 'file_import',
                        class: 'hidden',
                        accept: '.xls',
                        name: 'file_import_'+resource,
                    })
                )
            )
            .append(
                $('<div>', {
                    text: 'You can use this template',
                })
                .append(
                    $('<button>', {
                        class: 'btn btn-success block',
                        text: 'Download Template',
                        type: 'button',
                        'onClick': 'downloadTemplate()'
                    }).append($('<i>', {
                        class: 'fa fa-download mx-2',
                    }))
                )
            )
        )
        
    }

    function downloadTemplate() {
        window.open(
            siteUrl+ `assets/template/${resource}_template.xls`,
            '_blank'
        );
    }

    function toggleLoading() {
        $('#loading_section').toggleClass('hidden')
        $('#loading_section').toggleClass('flex')
    }

    $('#export_btn').click(e => {
        window.open(
            siteUrl+ `admin/cv/export/${resource}`,
            '_blank'
        );
    });


    $('#modal_close').click(() => {
        toggleModal();
    })

    $('#add_btn').click(() => {
        action = 'store'
        modalTitle.html('Add ' + resource)
        setInputModal(modalInputs[resource]);
        toggleModal()
    })

    $('#import_btn').click(() => {
        action = 'import_file'
        setImportModal()
        toggleModal()
    });

    $('.tab').click(function() {
        resource = $(this).attr('id').replace('-tab', '')
        $('#table_title').text(resource)

        console.log($(this).attr('data-tabs-target'));
        if ($(this).attr('data-tabs-target') == '#table_section') {
            setTable();
        }
    });

    form.submit(e => {
        e.preventDefault();
        resetErrorMessageModal(modalInputs[resource])
        var formSerialize = form.serialize()

        if (action == 'store') {
            $.post(siteUrl + `admin/cv/store/${resource}`, formSerialize)
                .done(ajaxDoneCallback)
                .fail(ajaxFailCallback)

            refreshDatatable()
        } else if (action == 'edit') {
            $.post(siteUrl + `admin/cv/update/${resource}/${activeId}`, formSerialize)
                .done(ajaxDoneCallback)
                .fail(ajaxFailCallback)

            activeId = null;
            refreshDatatable()
        } else if (action == 'import_file') {
            var formData = new FormData(form[0])

            $.ajax({
                type: 'POST',
                url: siteUrl + `admin/cv/import/${resource}`,
                enctype: 'multipart/form-data',
                contentType: false,
                processData: false,
                data: formData,
                success: response => {
                    if (response.status == 'success') {
                        toggleModal();

                        Toast.fire({
                            icon: response.status,
                            title: response.message,
                        })

                        if (response.kicked_message.length > 0) {
                            var ul = $('<ul>')

                            response.kicked_message.forEach(message => {
                                ul.append($('<li>', {
                                    text: message
                                }))
                            })

                            setTimeout(() => { 
                                Swal.fire({
                                    title: 'Some data not inserted',
                                    icon: 'info',
                                    html: ul.html(),
                                    showCloseButton: true,
                                    showCancelButton: true,
                                    focusConfirm: false,
                                })
                            }, 1500);
                        }

                        refreshDatatable()
                    } else if (response.status == 'error') {
                        Toast.fire({
                            icon: 'error',
                            title: response.message,
                        })
                    }
                },
                error: () => {
                }
            })
        }
        

    })
</script>
<script src="/assets/js/profile/general.js"></script>
<!-- <script src="/assets/js/profile/cv.js"></script> -->

<script>
</script>

</html>