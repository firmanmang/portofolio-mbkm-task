<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <title>Create Post</title>
    <!--     Fonts and icons     -->
    <!---    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />    --->
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="/assets/fonts/fontawesome/css/font-awesome.min.css">
    <!-- Nucleo Icons -->
    <link href="/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Popper -->
    <!-- <script src="https://unpkg.com/@popperjs/core@2"></script> -->
    <!-- Main Styling -->
    <link href="/assets/css/argon-dashboard-tailwind.css" rel="stylesheet" />

    <script src="/assets/js/jquery-1.min.js"></script>
    <script src="/assets/js/swal2.min.js"></script>

</head>

<body class="m-0 font-sans text-base antialiased font-normal dark:bg-slate-900 leading-default bg-gray-50 text-slate-500">
    <div class="absolute w-full bg-blue-500 dark:hidden min-h-75"></div>
    <?php component('sidenav') ?>

    <main class="relative h-full max-h-screen transition-all duration-200 ease-in-out xl:ml-68 rounded-xl">

        <?php component('navbar', ['title' => 'post']) ?>

        <!-- Section -->

        <div class="min-h-screen w-full px-6 py-6 mx-auto">
            <div class="flex flex-wrap -mx-3">
                <div class="flex-none w-full max-w-full px-3">
                    <a href="<?= site_url('admin/post') ?>" class="btn bg-gray-200 hover:bg-white mb-10">
                        <span class="fa fa-chevron-left mr-4"></span> Back
                    </a>

                    <div class="p-8 relative flex flex-col w-full lg:w-2/3 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
                        <!-- Heading -->
                        <div class="">
                            <div class="flex flex-col justify-center w-fit">
                                <div class="pb-0 mb-0 border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
                                    <h6 class="dark:text-white">Create Post</h6>
                                </div>
                            </div>
                        </div>
                        <!-- End Heading -->
                        <form id="form_input">
                            <div class="grid md:grid-cols-1 lg:grid-cols-3 gap-x-6 gap-y-3 ">
                                <div class="col-span-3">
                                    <div class="w-1/2 mb-4 mx-auto ">
                                        <img id="preview_img" class="w-full h-full rounded-md border shadow-xxs" src="<?= $post->img_feature ?>" alt="">
                                    </div>
                                    <div class="w-1/3 mx-auto">
                                        <label for="img_feature" class="inline-block mb-1 ml-1 font-bold text-xs text-slate-700 dark:text-white/80">Image</label>
                                        <input value="" type="file"  name="img_feature" id="img_feature" class="input w-full"  accept="image/png, image/jpeg">
                                        <p id="img_feature_error" class="text-sm text-red-500"></p>
                                    </div>
                                </div>
                                <div class="w-full">
                                    <label for="title" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">
                                        title
                                    </label>
                                    <input value="<?= $post->title ?>" type="text" name="title" id="title" class="input w-full" />
                                    <p id="title_error" class="text-sm text-red-500"></p>
                                </div>
                                <div class="w-full">
                                    <label for="slug" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">
                                        slug
                                    </label>
                                    <input value="<?= $post->slug ?>" type="text" name="slug" id="slug" class="input w-full" />
                                    <p id="slug_error" class="text-sm text-red-500"></p>
                                </div>
                                <div class="w-full">
                                    <label for="category_id" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">
                                        category
                                    </label>
                                    <select value="" type="text" name="category_id" id="category_id" class="input w-full">
                                        <option selected disabled></option>
                                    </select>
                                    <p id="category_id_error" class="text-sm text-red-500"></p>
                                </div>
                                <div class="col-span-3 w-full">
                                    <label for="content" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">
                                        content
                                    </label>
                                    <textarea class="input w-full" name="content" id="content" cols="30" rows="10"><?= $post->content ?></textarea>
                                    <p id="content_error" class="text-sm text-red-500"></p>
                                </div>
                                <div class="">
                                    <button type="submit" class="btn btn-primary w-full">SAVE</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- End Section -->

        <?php component('footer') ?>
        </div>
        <!-- end cards -->
    </main>

    <?php component('right-conf') ?>
    <script src="/assets/js/main.js"></script>
    <script>
        let imgInp = $('#img_feature');
        let previewImg = $('#preview_img');
        

        let inputs = {
            img_feature: $('#img_feature'),
            title: $('#title'),
            slug: $('#slug'),
            category_id: $('#category_id'),
            content: $('#content'),
        }


        imgInp.change(evnt => {
            let file = event.target.files[0];
            previewImg.attr('src', URL.createObjectURL(file));
        });

        $(document).ready(function() {
            $.ajax({
                method: 'GET',
                url: '<?= site_url('admin/postcategory/get-all?status=active') ?>',
                success: function(result) {
                    if(result.status == 'success') {
                        result.data.forEach(category => {
                            $('#category_id').append($('<option>', {
                                value: category.id,
                                text: category.category_name
                            }));
                        });

                        $('#category_id').val(<?= $post->category_id  ?>);
                    }
                }
            });
        });

        $('#form_input').submit(function(e) {
            e.preventDefault();
            resetErrorMessage(inputs);

            let formData = new FormData($(this)[0]);

            $.ajax({
            type: 'POST',
            url: '<?= site_url('admin/post/update/'.$post->id) ?>',
            enctype: 'multipart/form-data',
            contentType: false,
            processData: false,
            data: formData,
            success: function(result, status) {
                resetErrorMessage(inputs);

                if (result.status == 'success') {
                    window.location.href = '<?= site_url('admin/post') ?>?message='+result.message;
                } else if (result.status == 'validation-error'){
                    for (var key in result.errors) {
                        $('#'+key+'_error').html(result.errors[key]);
                    }

                } else {
                    Toast.fire({
                        icon: 'error',
                        title: 'Sorry, failed to update post',
                    })
                }
            },
            error: function() {
                Toast.fire({
                    icon: 'error',
                    title: 'Sorry, failed to update post',
                })
            }
        })
        });
    </script>
</body>


</html>