<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <title><?= strtoupper($post->title) ?></title>
    <!--     Fonts and icons     -->
    <!---    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />    --->
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="/assets/fonts/fontawesome/css/font-awesome.min.css">
    <!-- Nucleo Icons -->
    <link href="/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Popper -->
    <!-- <script src="https://unpkg.com/@popperjs/core@2"></script> -->
    <!-- Main Styling -->
    <link href="/assets/css/argon-dashboard-tailwind.css" rel="stylesheet" />

    <script src="/assets/js/jquery-1.min.js"></script>
    <script src="/assets/js/swal2.min.js"></script>

</head>

<body class="m-0 font-sans text-base antialiased font-normal dark:bg-slate-900 leading-default bg-gray-50 text-slate-500">
    <div class="absolute w-full bg-blue-500 dark:hidden min-h-75"></div>
    <?php component('sidenav') ?>

    <main class="relative h-full max-h-screen transition-all duration-200 ease-in-out xl:ml-68 rounded-xl">

        <?php component('navbar', ['title' => 'post']) ?>

        <!-- Section -->

        <div class="min-h-screen w-full px-6 py-6 mx-auto">
            <div class="flex flex-wrap -mx-3">
                <div class="flex-none w-full max-w-full px-3">
                    <a href="<?= site_url('admin/post') ?>" class="btn bg-gray-200 hover:bg-white mb-10">
                        <span class="fa fa-chevron-left mr-4"></span> Back
                    </a>
                    <div class="p-8 relative flex flex-col w-full lg:w-2/3 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
                        <!-- Heading -->
                        <div class="w-full">
                            <div class="pb-0 mb-0 border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
                                <h6 class="dark:text-white capitalize text-center text-2xl"><?= $post->title ?></h6>
                            </div>
                        </div>
                        <!-- End Heading -->
                        <div class="flex flex-col gap-4">
                            <div class="w-full my-5">
                                <img id="preview_img" class="w-full h-full rounded-md border shadow-xxs" src="<?= $post->img_feature ?>" alt="">
                            </div>
                            <div class="w-full">
                                <label for="slug" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">
                                    slug
                                </label>
                                <input value="<?= $post->slug ?>" type="text" disabled name="slug" id="slug" class="bg-transparent border-0 w-full p-1" />
                            </div>
                            <div class="w-full">
                                <label for="category_id" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">
                                    category
                                </label>
                                <input value="<?= $post->category_name ?>" type="text" disabled name="slug" id="slug" class="bg-transparent border-0 w-full p-1" />
                            </div>
                            <div class="col-span-3 w-full">
                                <label for="content" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">
                                    content
                                </label>
                                <p class="bg-transparent border-0 w-full p-1 min-h-28" disabled name="content" id="content">
                                    <?= $post->content ?>
                                </p>
                                <p id="content_error" class="text-sm text-red-500"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- End Section -->

        <?php component('footer') ?>
        </div>
        <!-- end cards -->
    </main>

    <?php component('right-conf') ?>

</body>


</html>