<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <title>Post Category</title>
    <!--     Fonts and icons     -->
    <!---    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />    --->
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="/assets/fonts/fontawesome/css/font-awesome.min.css">
    <!-- Nucleo Icons -->
    <link href="/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Popper -->
    <!-- <script src="https://unpkg.com/@popperjs/core@2"></script> -->
    <!-- Main Styling -->
    <link href="/assets/css/argon-dashboard-tailwind.css" rel="stylesheet" />
    
    <script src="/assets/js/jquery-1.min.js"></script>
    <script src="/assets/js/swal2.min.js"></script>
    
    <!-- <script src="https://code.jquery.com/jquery-3.6.1.slim.js" integrity="sha256-tXm+sa1uzsbFnbXt8GJqsgi2Tw+m4BLGDof6eUPjbtk=" crossorigin="anonymous"></script> -->
    <script src="/assets/js/datatables.min.js"></script>
    <!-- <link rel="stylesheet" href="/assets/css/datatables.min.css"> -->
</head>

<body class="m-0 font-sans text-base antialiased font-normal dark:bg-slate-900 leading-default bg-gray-50 text-slate-500">
    <div class="absolute w-full bg-blue-500 dark:hidden min-h-75"></div>
    <?php component('sidenav') ?>

    <main class="relative h-full max-h-screen transition-all duration-200 ease-in-out xl:ml-68 rounded-xl">

        <?php component('navbar', ['title' => 'post']) ?>
        <!-- cards -->
        <div class="min-h-screen w-full px-6 py-6 mx-auto">
            <div class="flex flex-wrap -mx-3">
                <div class="flex-none w-full max-w-full px-3">
                    <div class="relative flex flex-col min-w-0 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
                        <!-- Heading -->
                        <div class="">
                            <div class="flex flex-col justify-center w-fit">
                                <div class="p-6 pb-0 mb-0 border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
                                    <h6 class="dark:text-white">Post table</h6>
                                </div>
                                <div class=" px-6 ">
                                    <a href="<?= site_url('admin/post/create') ?>" class="btn btn-primary"><i class="fa fa-plus mx-2"></i> Add</a>
                                </div>
                            </div>
                        </div>
                        <!-- End Heading -->
                        <!-- Tabel Content -->
                        <div class="flex-auto px-5 pt-0 pb-2">
                            <div class="p-4 overflow-x-auto">
                                <table id="table_post" class="mx-auto dark:text-white bg-gray-200 dark:bg-slate-800 col-span-2 p-4 rounded-2xl items-center w-full mb-0 align-top border-collapse dark:border-white/40 text-slate-500">
                                    <thead class="align-bottom">
                                        <tr>
                                            <th class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40 dark:text-white tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                Image
                                            </th>
                                            <th class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40 dark:text-white tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                Title
                                            </th>
                                            <th class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40 dark:text-white tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                Slug
                                            </th>
                                            <th class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40 dark:text-white tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                Category
                                            </th>
                                            <th class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40 dark:text-white tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                Writer
                                            </th>
                                            <th class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-collapse border-solid shadow-none dark:border-white/40 dark:text-white tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                Action
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>                                      
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- End Tabel Content -->

                    </div>
                </div>
            </div>

            <?php component('footer') ?>
        </div>
        <!-- end cards -->
    </main>

    <?php component('right-conf') ?>

    <script src="/assets/js/main.js"></script>
    <script>
        let params = new URLSearchParams(location.search);
        let message =  params.get('message');
        
        if (message != null) {
            Toast.fire({
                icon: 'success',
                title: message,
            })
        }

        var url = '<?= site_url('admin/post') ?>'; 
        let dataHelper = new DataHelper();
        let dataTable = $('#table_post').DataTable({
            ajax: getUrl('get-all'),
            order: [[1, 'asc']],
            serverSide: true,
            pageLength: 50,
            columns: [
                {data: 'img_feature'},
                {data: 'title'},
                {data: 'slug'},
                {data: 'category_name'},
                {data: 'writer'},
            ],
            columnDefs: [
                {
                    targets: 0,
                    data: 'img_feature',
                    orderable: false,
                    render: function (img_feature) {
                        return `
                            <img class="mx-auto h-12 max-w-20 w-auto py-1 rounded-lg" src="${img_feature}" alt="image post">
                        `;
                    }
                },
                {
                    targets: 5,
                    data: 'id',
                    render: function (id) {
                        return `
                        <a href="${getUrl('show/'+id)}" class="btn btn-sm btn-success mx-1"> 
                            <span class="fa fa-eye mr-1"></span>
                            Show
                        </a>
                        <a href="${getUrl('edit/'+id)}" class="btn btn-sm btn-warning mx-1"> 
                            <span class="fa fa-pencil mr-1"></span>
                            Edit
                        </a>
                        <button onClick="destroy(${id})" class="btn btn-sm btn-danger mx-1"> 
                            <span class="fa fa-trash mr-1"></span>
                            Delete
                        </button>`
                        ;
                }
            }]
        });

        function refreshDataTable() {
            dataTable.ajax.reload();
        }
        
        
        function getUrl(path = '') {
            return url+'/'+path
        }

        function destroy(id) {
            $.get(getUrl('get/'+id))
                .done(function(result) {
                    if (result.data == null) {
                        Toast.fire({
                            icon: 'error',
                            message: 'Sorry, failed to delete this post'
                        });
                    } else {
                        let data = result.data;
                        Swal.fire({
                            title: 'Are you sure?',
                            text: 'Do you want delete '+data.title+'?',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#60a5fa',
                            cancelButtonColor: '#f53939',
                            confirmButtonText: 'Yes, delete it!'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $.post(getUrl('delete/'+id))
                                    .done(function() {
                                        Toast.fire({
                                            icon: 'success', 
                                            title: 'Post '+ data.title +' has been deleted.'
                                        });
                                        refreshDataTable();
                                    })
                            }
                        })
                    }
                })
                .fail(function() {
                    Toast.fire({
                        icon: 'error',
                        title: 'Sorry, failed to delete this post'
                    });
                });
        }

    </script>

</body>
<!-- plugin for charts  -->
<script src="/assets/js/plugins/chartjs.min.js"></script>
<!-- plugin for scrollbar  -->
<script src="/assets/js/plugins/perfect-scrollbar.min.js"></script>
<!-- main script file  -->
<script src="/assets/js/argon-dashboard-tailwind.js?v=1.0.1"></script>

</html>