<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <title>Create Post</title>
    <!--     Fonts and icons     -->
    <!---    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />    --->
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="/assets/fonts/fontawesome/css/font-awesome.min.css">
    <!-- Nucleo Icons -->
    <link href="/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Popper -->
    <!-- <script src="https://unpkg.com/@popperjs/core@2"></script> -->
    <!-- Main Styling -->
    <link rel="stylesheet" href="/assets/css/select2.min.css">
    <link href="/assets/css/argon-dashboard-tailwind.css" rel="stylesheet" />

    <script src="/assets/js/jquery-1.min.js"></script>
    <script src="/assets/js/swal2.min.js"></script>
    <script src="/assets/js/select2.min.js"></script>


</head>

<body class="m-0 font-sans text-base antialiased font-normal dark:bg-slate-900 leading-default bg-gray-50 text-slate-500">
    <div class="absolute w-full bg-blue-500 dark:hidden min-h-75"></div>
    <?php component('sidenav') ?>

    <main class="relative h-full max-h-screen transition-all duration-200 ease-in-out xl:ml-68 rounded-xl">

        <?php component('navbar', ['title' => 'post']) ?>

        <!-- Section -->

        <div class="min-h-screen w-full px-6 py-6 mx-auto">
            <div class="flex flex-wrap -mx-3">
                <div class="flex-none w-full max-w-full px-3">
                    <a href="<?= site_url('admin/work') ?>" class="btn bg-gray-200 hover:bg-white mb-10">
                        <span class="fa fa-chevron-left mr-4"></span> Back
                    </a>
                    <div class="p-8 relative flex flex-col w-full lg:w-2/3 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-xl dark:bg-slate-850 dark:shadow-dark-xl rounded-2xl bg-clip-border">
                        <!-- Heading -->
                        <div class="">
                            <div class="flex flex-col justify-center w-fit">
                                <div class="pb-0 mb-0 border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
                                    <h6 class="dark:text-white">Update Post</h6>
                                </div>
                            </div>
                        </div>
                        <!-- End Heading -->
                        <form id="form_input">
                            <div class="grid md:grid-cols-1 lg:grid-cols-3 gap-x-6 gap-y-3 ">
                                <div class="w-full">
                                    <label for="work" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">
                                        work
                                    </label>
                                    <input type="text" name="work" id="work" class="input w-full" />
                                    <p id="work_error" class="text-sm text-red-500"></p>
                                </div>
                                <div class="w-full">
                                    <label for="year" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">
                                        year
                                    </label>
                                    <input type="text" name="year" id="year" class="input w-full" />
                                    <p id="year_error" class="text-sm text-red-500"></p>
                                </div>
                                <div class="w-full">
                                    <label for="city" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">
                                        city
                                    </label>
                                    <input type="text" name="city" id="city" class="input w-full" />
                                    <p id="city_error" class="text-sm text-red-500"></p>
                                </div>
                                <div class="w-full">
                                    <label for="position" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">
                                        position
                                    </label>
                                    <input type="text" name="position" id="position" class="input w-full" />
                                    <p id="position_error" class="text-sm text-red-500"></p>
                                </div>
                                <div class="w-full">
                                    <label class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">
                                        category
                                    </label>
                                    <select class="input w-full" name="categories_id[]" id="categories_id" multiple="multiple">
                                        <option value=""></option>
                                    </select>
                                    <div id="categories_id_container" class="grid md:grid-cols-2 lg:grid-cols-3 gap-2 w-full p-2"></div>
                                    <p id="categories_id_error" class="text-sm text-red-500"></p>
                                </div>
                                <div class="col-span-3 w-full">
                                    <label for="description" class="inline-block mb-2 ml-1 font-bold text-xs text-slate-700 dark:text-white/80 capitalize">
                                        description
                                    </label>
                                    <textarea class="input w-full" name="description" id="description" cols="30" rows="10">
                                    </textarea>
                                    <p id="description_error" class="text-sm text-red-500"></p>
                                </div>
                                <div class="">
                                    <button type="submit" class="btn btn-primary w-full">SAVE</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- End Section -->

        <?php component('footer') ?>
        </div>
        <!-- end cards -->
    </main>

    <?php component('right-conf') ?>
    <script src="/assets/js/main.js"></script>
    <script>
        let inputs = {
            work: $('#work'),
            year: $('#year'),
            city: $('#city'),
            position: $('#position'),
            categories_id: $('#categories_id'),
            description: $('#description'),
        }
        let id = '<?= $id ?>';

        function setSelectedCategory(categories) {
            categories.forEach(function(category) {
                $(`input[type=checkbox][value=${category.id}]`).prop('checked', true);
            });
        }

        async function getCategories() {
            await $.ajax({
                method: 'GET',
                url: '<?= site_url('admin/workcategory/get-all') ?>',
                success: function(result) {
                    if (result.status == 'success') {
                        result.data.forEach(category => {
                            let wrap = $('<div>', {
                                    class: 'flex flex-row'
                                })
                                .append($('<input />', {
                                    class: 'checkbox',
                                    type: 'checkbox',
                                    id: 'category_' + category.id,
                                    name: 'categories_id[]',
                                    value: category.id,
                                }))
                                .append($('<label>', {
                                    for: 'category_' + category.id,
                                    text: category.name
                                }))

                            $('#categories_id').append($('<option>', {
                                value: category.id,
                                text: category.name,
                                id: 'category_' + category.id,
                            }));

                            // $('#categories_id_container').append(wrap);
                        });
                        $('#categories_id').select2();
                    }
                }
            });
        }

        function insertData() {
            $.get('<?= site_url('admin/work/get/') ?>' + id).done(result => {
                let work = result.data;
                let workCategories = work.categories;
                for (key in work) {
                    if (typeof work[key] == 'string') {
                        $('#' + key).val(work[key]);
                    }
                }

                $('#categories_id').val(workCategories)
                    .trigger('change'); 
            });

        }

        $(document).ready(function() {
            getCategories().then(e => {
                insertData();
            })
        });

        $('#form_input').submit(function(e) {
            e.preventDefault();
            resetErrorMessage(inputs);

            let formData = $(this).serialize();

            $.ajax({
                type: 'POST',
                url: '<?= site_url('admin/work/update/') ?>' + id,
                data: formData,
                success: function(result, status) {
                    resetErrorMessage(inputs);

                    if (result.status == 'success') {
                        window.location.href = '<?= site_url('admin/work') ?>?message=' + result.message;
                    } else if (result.status == 'validation-error') {
                        for (var key in result.errors) {
                            let id = key.replace('[]', '');
                            $('#' + id + '_error').html(result.errors[key]);
                        }

                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: 'Sorry, failed to store work',
                        })
                    }
                },
                error: function() {
                    Toast.fire({
                        icon: 'error',
                        title: 'Sorry, failed to store work',
                    })
                }
            })
        });
    </script>
</body>


</html>