<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <!-- <link rel="stylesheet" href="/assets/css/argon-dashboard-tailwind.css"> -->
    <link rel="stylesheet" href="<?= FCPATH.'/assets/css/cv.css' ?>">
</head>

<body class="m-0 font-sans text-base antialiased font-normal dark:bg-slate-900 leading-default bg-gray-50 text-slate-500">
    <div id="cv_section" class="mx-auto w-full flex column-1 gap-8 rounded-lg border p-16 text-gray-700 shadow-lg">
        <table>
            <tr>
                <td style="vertical-align: top;width: 100%; padding-right: 3rem;">
                    <img class="w-img h-auto rounded-full" src="<?= FCPATH.$user->image_profile ?>" alt="" />
                </td>
                <td>
                    <div class="">
                        <h1 id="cv_fullname" class="text-3xl font-bold capitalize"><?= $user->name ?></h1>
                        <div class="">
                            <h5 class="text-gray-400 text-xs">Email: <span id="cv_email"><?= $user->email ?></span></h5>
                            <h5 class="text-gray-400 text-xs">Phone: <span id="cv_phone"><?= $user->phone_number ?></span></h5>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <hr class="h-[2px] bg-gradient-to-r from-gray-200 via-gray-400 to-gray-200" />
        <!-- Experience -->
        <div class="">
            <h2 style="margin: 0;" class="text-xl capitalize">experience</h2>
            <div style="margin: 0;" id="cv_experience" class="text-sm">
                <table>
                    <?php foreach ($experiences as $experience) : ?>
                    <tr>
                        <td style="vertical-align: top;width: 35%; ">
                            <?= $experience->start_date . ' - ' . $experience->resign_date ?>
                        </td>
                        <td class="">
                            <h4 class="mb-2 text-xl font-bold">
                                <?= $experience->name ?>
                            </h4>
                            <p class="text-xs text-gray-400">
                                <?= $experience->description ?>
                            </p>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
        <hr class="h-[2px] bg-gradient-to-r from-gray-200 via-gray-400 to-gray-200" />
        <!-- Education -->
        <div class="">
            <h2 class="text-xl capitalize">education</h2>
            <div class="px-2 text-sm">
                <ul id="cv_education" class="list-disc">
                    <?php foreach ($educations as $education) : ?>
                        <li>
                            <div class="">
                                <p class="m-0 font-bold">
                                    <?= $education->name ?>
                                </p>
                                <p class="m-0 text-xs">
                                    <?= $education->start_date . ' - ' . $education->graduate_date ?>
                                </p>
                                <p class="m-0 text-xs">
                                    <?= $education->description ?>
                                </p>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <hr class="h-[2px] bg-gradient-to-r from-gray-200 via-gray-400 to-gray-200" />

        <section>
            <table style="width: 100%; " >
                <tr>
                    <td>
                        <h2 class="text-xl capitalize">Hobby</h2>
                    </td>
                    <td>
                        <h2 class="text-xl capitalize">language</h2>
                    </td>
                    <td>
                        <h2 class="text-xl capitalize">Skill</h2>
                    </td>
                    <td>
                        <h2 class="text-xl capitalize">social media</h2>
                    </td>
                </tr>
                <tr style="width: 100%;" >   
                    <!-- Hobby -->
                    <td style="vertical-align: top;width: 24%;" class="">
                            <ul style="margin-left: -1.5rem;"  id="cv_hobby" class="">
                                <?php foreach ($hobbies as $hobby) : ?>
                                    <li class="capitalize"><?= $hobby->hobby ?></li>
                                <?php endforeach; ?>
                            </ul>
                    </td>
                    <!-- Language -->
                    <td style="vertical-align: top;width: 22%;" class="">
                        <div class="">
                            <ul  style="margin-left: -1.5rem;"  id="cv_language" class="grid gap-3">
                                <?php foreach ($languages as $language) : ?>
                                    <li class="capitalize">
                                        <?= $language->language ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </td>
                    <!-- Skill -->
                    <td style="vertical-align: top;width: 24%;" class="">
                        <div class="">
                            <ul  style="margin-left: -1.5rem;"  id="cv_skill" class="grid gap-3">
                                <?php foreach ($skills as $skill) : ?>
                                    <li class="capitalize">
                                        <?= $skill->name ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </td>
                    <!-- Social Media -->
                    <td style="vertical-align: top;width: 27%;" class="">
                        <div class="">
                            <ul  style="margin-left: -1.5rem;"  id="cv_socialmedia" class="grid gap-1">
                                <?php foreach ($socialmedias as $socialmedia) : ?>
                                    <li><?= $socialmedia->name . ' : ' . $socialmedia->url ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </td>
                </tr>
            </table>
        </section>
    </div>


    <!-- <script src="/assets/js/profile/cv.js"></script> -->

</html>