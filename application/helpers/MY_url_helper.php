<?php

/**
 * isActive
 * Checking current url equals with parameter
 * @param  string $route
 * @return bool
 */
function isActive($route): bool
{
    return (current_url() === site_url($route));
}

