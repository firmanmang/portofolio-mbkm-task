<?php



/**
 * inputToArrayAssoc
 * Filter array Associative by columns on a model as representation a table in database
 * @param  array $input
 * @param  CI_Model $model
 * @return array
 */
function inputToArrayAssoc($input, $model)
{
    $columns = $model->columns;
    $result = [];
    
    foreach ($columns as $column) {
        if (isset($input[$column])) {
            $result[$column] = $input[$column];
        }
    }

    return $result;
}