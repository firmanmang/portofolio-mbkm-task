<?php

/**
 * component
 * load a view (component) 
 * @param  string $path
 * @param  mixed $viewData
 * @return void
 */
function component($path, $viewData = null) {
    require APPPATH .'views/components/'.$path.'.php';
}