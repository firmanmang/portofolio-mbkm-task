<?php

/**
 * jsonOutput
 * Function to tranform a array to http response json
 * @param  CI_Controler $obj
 * @param  array $content
 * @return CI_Output
 */
function jsonOutput($obj, $content)
{
    return $obj->output
        ->set_content_type('application/json')
        ->set_output(json_encode($content));
}

/**
 * dd
 * Debug function to dump value and exit/die code
 * @param  mixed $arg
 * @return void
 */
function dd($arg)
{
    echo '<pre>';
    print_r($arg);
    die();
}



/**
 * setValueAsKey
 * set a value in array[] as it's key, if the key not uniq the last data selected
 * @param  string $keyColumn
 * @param  array[][] $data
 * @return array[][]
 */
function setValueAsKey($keyColumn, $data)
{
    $result = [];

    foreach ($data as $d) {
        $key = $d[$keyColumn];
        $result[$key] = $d;
    }

    return $result;
}



/**
 * objectToArray
 * tranform a array Associative to array indexed
 * @param  array $data
 * @return array
 */
function objectToArray($data)
{
    return json_decode(json_encode($data), true);
}
