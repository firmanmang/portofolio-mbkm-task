<?php

$config = [
    'login' => [
        [
            'field' => 'identifier',
            'label' => 'Username/Email',
            'rules' => 'required',
            // 'errors' => [
            //     'required' => '%s harus diisi',
            //     'is_unique' => '%s sudah dipakai',
            // ],
        ],
        [
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required',
            // 'errors' => [
            //     'required' => '%s harus diisi',
            // ],
        ],
    ],
    'register' => [
        [
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'required',
            // 'errors' => [
            //     'required' => '%s harus diisi',
            // ],
        ],
        [
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required|is_unique[user.username]|alpha_dash',
            // 'errors' => [
            //     'required' => '%s harus diisi',
            //     'is_unique' => '%s sudah dipakai',
            // ],
        ],
        [
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|is_unique[user.email]|valid_email',
            // 'errors' => [
            //     'required' => '%s harus diisi',
            //     'is_unique' => '%s sudah dipakai',
            // ],
        ],
        [
            'field' => 'phone_number',
            'label' => 'Phone Number',
            'rules' => 'required|regex_match[/^0[0-9]{10,14}$/]',
            // 'errors' => [
            //     'required' => '%s harus diisi',
            // ],
        ],
        [
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|min_length[8]',
            // 'errors' => [
            //     'required' => '%s harus diisi',
            // ],
        ],
        [
            'field' => 'retype_password',
            'label' => 'Retype Password',
            'rules' => 'required|matches[password]',
            // 'errors' => [
            //     'required' => '%s harus diisi',
            // ],
        ],
    ],
    'update-profile' => [
        [
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'required',
            // 'errors' => [
            //     'required' => '%s harus diisi',
            // ],
        ],
        [
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required|alpha_dash',
            // 'errors' => [
            //     'required' => '%s harus diisi',
            //     'is_unique' => '%s sudah dipakai',
            // ],
        ],
        [
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|valid_email',
            // 'errors' => [
            //     'required' => '%s harus diisi',
            //     'is_unique' => '%s sudah dipakai',
            // ],
        ],
        [
            'field' => 'phone_number',
            'label' => 'Phone Number',
            'rules' => 'required|regex_match[/^0[0-9]{10,14}$/]',
            // 'errors' => [
            //     'required' => '%s harus diisi',
            // ],
        ],
    ],
    'change-password' => [
        [
            'field' => 'current_password',
            'label' => 'Current Password',
            'rules' => 'required',
            // 'errors' => [
            //     'required' => '%s harus diisi',
            // ],
        ],
        [
            'field' => 'new_password',
            'label' => 'New Password',
            'rules' => 'required|min_length[8]',
            // 'errors' => [
            //     'required' => '%s harus diisi',
            // ],
        ],
        [
            'field' => 'retype_password',
            'label' => 'Re-type New Password',
            'rules' => 'required|matches[new_password]',
        ],
    ],
    'post/store' => [
        [
            'field' => 'title',
            'label' => 'title',
            'rules' => 'required',
        ],
        [
            'field' => 'slug',
            'label' => 'slug',
            'rules' => 'required|is_unique[posts.slug]',
        ],
        [
            'field' => 'category_id',
            'label' => 'category',
            'rules' => 'required',
        ],
        [
            'field' => 'content',
            'label' => 'content',
            'rules' => 'required',
        ],
    ],
    'post/update' => [
        [
            'field' => 'title',
            'label' => 'title',
            'rules' => 'required',
        ],
        [
            'field' => 'slug',
            'label' => 'slug',
            'rules' => 'required',
        ],
        [
            'field' => 'category_id',
            'label' => 'category',
            'rules' => 'required',
        ],
        [
            'field' => 'content',
            'label' => 'content',
            'rules' => 'required',
        ],
    ],
    'work/store' => [
        [
            'field' => 'work',
            'label' => 'work',
            'rules' => 'required',
        ],
        [
            'field' => 'year',
            'label' => 'year',
            'rules' => 'required',
        ],
        [
            'field' => 'city',
            'label' => 'city',
            'rules' => 'required',
        ],
        [
            'field' => 'position',
            'label' => 'position',
            'rules' => 'required',
        ],
        [
            'field' => 'description',
            'label' => 'description',
            'rules' => 'required',
        ],
        [
            'field' => 'categories_id[]',
            'label' => 'Category',
            'rules' => 'required'
        ],
    ],
];
