<?php

class Post extends CI_Controller
{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('post_model');
    }

    public function get($id)
    {
        $post = $this->post_model->get($id);
        return jsonOutput($this, [
            'status' => 'success',
            'data' => $post,
        ]);
    }
    
}