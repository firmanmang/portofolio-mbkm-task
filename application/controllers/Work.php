<?php

class Work extends CI_Controller
{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('work_model');
    }
    
    public function index()
    {
        $data['posts'] = $this->work_model->getGeneral();
        $this->load->view('works', $data);
    }

    public function get($id)
    {
        $work = $this->work_model->get($id);
        $work['categories'] = $this->work_model->getCategories($id)->categories;
        return jsonOutput($this, [
            'status' => 'success',
            'data' => $work,
        ]);
    }
}