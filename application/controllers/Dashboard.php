<?php

class Dashboard extends CI_Controller
{

    public function __construct() {
        parent::__construct();

        if (! $this->session->has_userdata('user_id')) {
            $this->session->set_flashdata('message', 'Silahkan login dulu');
            redirect('auth/login');
        }
    }

    public function index()
    {
        $this->load->view('admin/dashboard');
    }
}