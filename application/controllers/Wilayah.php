<?php
class Wilayah extends CI_Controller 
{

    public function __construct() {
        parent::__construct();

        $this->load->model('kabupaten_model');
		$this->load->model('kecamatan_model');
		$this->load->model('desa_model');
	}

    public function kabupaten($provinsiId = 0)
    {
        $kabupatenList = $this->kabupaten_model->pick('id, nama',['provinsi_id' => $provinsiId]);

        return jsonOutput($this, [
            'status' => 'success',
            'kabupaten' =>   $kabupatenList]);
    }

    public function kecamatan($kabupatenId = 0)
    {
        $kecamatanList = $this->kecamatan_model->pick('id, nama',['kabupaten_id' => $kabupatenId]);

        return jsonOutput($this, [
            'status' => 'success',
            'kecamatan' =>   $kecamatanList]);
    }

    public function desa($kecamatanId = 0)
    {
        $desaList = $this->desa_model->pick('id, nama',['kecamatan_id' => $kecamatanId]);

        return jsonOutput($this, [
            'status' => 'success',
            'desa' =>   $desaList]);
    }
}