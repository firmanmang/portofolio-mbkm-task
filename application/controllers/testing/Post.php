<?php

class Post extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('grocery_CRUD');    
    }

    public function index()
    {
        $this->load->helper('string');
        $imageName = '';
        $imagepath = 'assets/uploads/posts/';

        $crud = new Grocery_CRUD();

        $crud->set_table('posts');
        $crud->set_subject('post');
        
        $crud->columns('title', 'slug', 'img_feature',  'created_by', 'category_id');
        $crud->fields('title', 'slug', 'content', 'img_feature');

        $crud->set_field_upload('img_feature', $imagepath);
        $crud->callback_after_upload(function($uploader_response, $field_info, $files_to_upload) 
                use ($imageName, $imagepath) {
            $imageName = $uploader_response[0]->name;
        });
        
        $crud->set_relation('created_by', 'user', 'name');
        $crud->set_relation('category_id', 'post_categories', 'category_name');
        
        $crud->required_fields('title', 'slug', 'content', 'img_feature');
        $crud->unique_fields(['slug']);

        $crud->display_as('title', 'Title');
        $crud->display_as('slug', 'Slug');
        $crud->display_as('content', 'Content');
        $crud->display_as('created_by', 'Writer');
        $crud->display_as('category_id', 'Category Name');
        $crud->display_as('img_feature', 'Image');
        
        // $crud->unset_read('last_modified');

        // $crud->unset_texteditor('content');  
        $crud->unset_texteditor('img_feature');

        $crud->callback_insert(function($postArray) use ($imageName) {
            $postArray['category_id'] = 4;
            $postArray['created_by'] = $this->session->userdata('user_id');
            $this->load->model('post_model');
            $postArray = inputToArrayAssoc($postArray, $this->post_model);
            $this->post_model->store($postArray);
            return $postArray;
        });



        $output = $crud->render();

        $this->load->view('testing/post-index', $output);
    }
}