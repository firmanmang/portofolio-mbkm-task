<?php

use \ReCaptcha\ReCaptcha;

class Login extends CI_Controller
{
    public function __construct() {
        parent::__construct();

        if ($this->session->has_userdata('user_id')) {
            redirect('dashboard');
        }
    }

    public function index()
    {
        $this->load->view('auth/login');
    }

    public function store()
    {
        $inputForm  = $this->input->post();
        $secret     = RECAPTCHA_SECRETKEY;
        $site       = $inputForm['g-recaptcha-response'];

        $recaptcha = new ReCaptcha($secret);
        $resp = $recaptcha->setExpectedHostname($_SERVER['SERVER_NAME'])
                        ->verify($site, $_SERVER['REMOTE_ADDR']);
        
        if (! $resp->isSuccess()) {
            return jsonOutput($this, [
                'status' => 'captcha-error',
                'message' => 'please make sure the captcha is correct',
            ]);
        }

        if (! $this->form_validation->run('login')) {
            $errors = $this->form_validation->error_array();

            return jsonOutput($this, [
                    'status' => 'validation-error',
                    'message' => 'validation error',
                    'errors' => $errors,
                ]);
        }

        $this->load->model('user_model');
        $user = $this->user_model->login($inputForm);


        if ($user == null) {
            return jsonOutput($this, [
                    'status' => 'user-not-found',
                    'message' => 'user not found',
                ]);
        }

        $this->session->set_userdata('user_id', $user->id);
        $this->session->set_userdata('username', $user->username);
        return jsonOutput($this, [
                    'status' => 'success',
                    'message' => 'Success login',
                ]);
    }
}