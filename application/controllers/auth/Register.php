<?php

class register extends CI_Controller 
{
    public function __construct() {
        parent::__construct();

        if ($this->session->has_userdata('user_id')) {
            redirect('dashboard');
        }
    }

    public function index()
    {
        $this->load->view('auth/register');
    }

    public function store()
    {
        if (! $this->form_validation->run('register')) {
            $errors = $this->form_validation->error_array();
            return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode([
                    'status' => 'error',
                    'message' => 'validation error',
                    'errors' => $errors,
                ]));
        }

        $this->load->model('user_model');
        $inputForm = $this->input->post();
        $userData = inputToArrayAssoc($inputForm, $this->user_model);
        $this->user_model->register($userData);
        $this->session->set_flashdata('message', 'berhasil register');
        
        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode([
                'status' => 'success',
                'message' => 'registration complete'
            ]));

    }

}