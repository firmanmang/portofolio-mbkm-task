<?php
class Logout extends CI_Controller
{
    public function index()
    {
        if (! $this->session->has_userdata('user_id')) {
            redirect('auth/login');
        }

        $this->session->sess_destroy();
        $this->session->set_flashdata('message', 'berhasil logout');
        $this->load->view('auth/login');
    }
}