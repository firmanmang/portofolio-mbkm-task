<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->model('post_model');
		$this->load->model('work_model');
		$data['works'] = $this->post_model->getLastCreated(3);
		$data['posts'] = $this->work_model->getLastCreated();
		$this->load->view('welcome_message', $data);
	}

}
