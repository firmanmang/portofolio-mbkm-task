<?php

class Postcategory extends CI_Controller 
{
    public function __construct() {
        parent::__construct();
        $this->load->model('postcategory_model');
        $this->form_validation->set_rules('category_name', 'Category Name', 'required');

        if (! $this->session->has_userdata('user_id')) {
            $this->session->set_flashdata('message', 'Silahkan login dulu');
            redirect('auth/login');
        }
    }

    public function datatable()
    {
        $params = $this->input->get();
        return jsonOutput($this, $this->postcategory_model->getForDatatable($params));
    }

    public function index()
    {
        $data['categories'] = $this->postcategory_model->getAll();
        $this->load->view('admin/post-category', $data);
    }

    public function getAll()
    {      
        $data = [
            'status' => 'success',
            'data' => $this->postcategory_model->getAll()
        ];

        if($this->input->get('status') == 'active') {
            $data['data'] = $this->postcategory_model->getActive('id, category_name');
        }

        return jsonOutput($this, $data);
    }

    public function store()
    {
        if (! $this->form_validation->run()) {
            return jsonOutput($this, [
                'status' => 'validation-error',
                'errors' => $this->form_validation->error_array(),
            ]);
        }

        $data = [
            'category_name' => $this->input->post('category_name'),
            'is_active' => '1',
        ];

        if (! $this->postcategory_model->store($data)) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'Sorry, failed to add new category',
            ]);
        }

        return jsonOutput($this, [
            'status' => 'success',
            'message' => $data['category_name'].' saved',
        ]);
    }

    public function show($id)
    {
        $category = $this->postcategory_model->get($id);

        if($category == null) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'Sorry, this category not found',
            ]);
        }

        return jsonOutput($this, [
            'status' => 'success',
            'data' => $category,
        ]);
    }

    public function update($id)
    {
        
        $category = $this->postcategory_model->get($id);

        if($category == null) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'Sorry, this category not found',
            ]);
        }

        if (! $this->form_validation->run()) {
            return jsonOutput($this, [
                'status' => 'validation-error',
                'errors' => $this->form_validation->error_array(),
            ]);
        }

        $data = inputToArrayAssoc($this->input->post(), $this->postcategory_model);

        if (! $this->postcategory_model->update($id, $data)) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'Sorry, failed update this category. Maybe this category used',
            ]);
        }

        return jsonOutput($this, [
            'status' => 'success',
            'message' => $data['category_name'].' saved',
        ]);
    }
}