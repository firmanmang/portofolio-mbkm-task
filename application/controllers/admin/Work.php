<?php

class Work extends CI_Controller 
{
    private $errorMessages;

    public function __construct() {
        parent::__construct();
        $this->load->model('work_model');

        if (! $this->session->has_userdata('user_id')) {
            $this->session->set_flashdata('message', 'Silahkan login dulu');
            redirect('auth/login');
        }
    }
    
    public function index()
    {
        $this->load->view('admin/work/index');
    }

    public function get($id)
    {
        $work = $this->work_model->get($id);
        return jsonOutput($this, [
            'status' => 'success',
            'data' => $work,
        ]);
    }
    
    public function getAll()
    {
        $works = $this->work_model->getGeneral();

        return jsonOutput($this, [
            'status' => 'success',
            'data' => $works,
        ]);
    }

    public function create()
    {
        $this->load->view('admin/work/create');
    }

    public function show($id)
    {
        $data['id'] = $id;
        $this->load->view('admin/work/show', $data);
    }

    public function store()
    {
        if (! $this->form_validation->run()) {
            return jsonOutput($this, [
                'status' => 'validation-error',
                'errors' => $this->form_validation->error_array(),
            ]);
        }


        $inputData = $this->input->post();
       
        if (! $this->work_model->store($inputData)) {
            return jsonOutput($this, [
                'status' => 'error',
                'errors' => 'Sorry, Please retry to save',
            ]);
        }
        
        return jsonOutput($this, [
            'status' => 'success',
            'message' => $inputData['work'].' successfully saved',
        ]);
    }

    public function edit($id)
    {
        $data['id'] = $id;
        $this->load->view('admin/work/edit', $data);
    }

    public function update($id)
    {
        if (! $this->form_validation->run('work/store')) {
            return jsonOutput($this, [
                'status' => 'validation-error',
                'errors' => $this->form_validation->error_array(),
            ]);
        }

        $inputData = $this->input->post();
        $inputData['created_by'] = $this->session->userdata('user_id');

        if (! $this->work_model->update($id, $inputData)) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'Post not saved',
            ]);
        }

        return jsonOutput($this, [
            'status' => 'success',
            'message' => $inputData['work'].' successfully saved',
        ]);
    }


    public function delete($id)
    {
        $this->work_model->delete($id);
        return jsonOutput($this, [
            'status' => 'success',
            'message' => 'Successfully deleted',
        ]);
    }

}