<?php

use Dompdf\Dompdf;
use Dompdf\Options;


class Cv extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('education_model');
        $this->load->model('experience_model');
        $this->load->model('hobby_model');
        $this->load->model('language_model');
        $this->load->model('skill_model');
        $this->load->model('socialmedia_model');

        if (! $this->session->has_userdata('user_id')) {
            $this->session->set_flashdata('message', 'Silahkan login dulu');
            redirect('auth/login');
        }
    }

    public function index()
    {
        $id = $this->session->userdata('user_id');

        $this->load->model('user_model');
        $data['user'] = $this->user_model->get($id);
        $data['experiences']    = $this->experience_model->getAll();
        $data['educations']     = $this->education_model->getAll();
        $data['hobbies']        = $this->hobby_model->getAll();
        $data['languages']      = $this->language_model->getAll();
        $data['skills']         = $this->skill_model->getAll();
        $data['socialmedias']   = $this->socialmedia_model->getAll();
        $this->load->view('admin/cv', $data);
    }

    public function getAll($resourceName)
    {
        $modelName  = $resourceName . '_model';
        $model      = $this->$modelName;
        $data       = $model->getAll();

        return jsonOutput($this, $data);
    }

    public function get($resourceName, $id)
    {
        $modelName  = $resourceName . '_model';
        $model      = $this->$modelName;
        $data       = $model->get($id);

        return jsonOutput($this, [
            'message' => 'success',
            'data' => $data,
        ]);
    }

    public function store($resourceName)
    {
        $modelName  = $resourceName . '_model';
        $formInput  = $this->input->post();
        $model      = $this->$modelName;
        $storeData  = inputToArrayAssoc($formInput, $model);

        foreach ($storeData as $key => $data) {
            $label = str_replace('_', ' ', $key);
            $this->form_validation->set_rules($key, $label, 'required');
        }

        if (!$this->form_validation->run()) {
            return jsonOutput($this, [
                'status' => 'validation-error',
                'errors' => $this->form_validation->error_array(),
            ]);
        }

        if (!$model->store($storeData)) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'sorry, we have a problem',
            ]);
        }

        return jsonOutput($this, [
            'status' => 'success',
            'message' => $resourceName . ' saved',
        ]);
    }

    public function update($resourceName, $id)
    {
        $modelName  = $resourceName . '_model';
        $model      = $this->$modelName;
        $formInput  = $this->input->post();

        $storeData  = inputToArrayAssoc($formInput, $model);

        foreach ($storeData as $key => $data) {
            $label = str_replace('_', ' ', $key);
            $this->form_validation->set_rules($key, $label, 'required');
        }

        if (!$this->form_validation->run()) {
            return jsonOutput($this, [
                'status' => 'validation-error',
                'errors' => $this->form_validation->error_array(),
            ]);
        }

        if (!$model->update($id, $storeData)) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'sorry, we have a problem',
            ]);
        }

        return jsonOutput($this, [
            'status' => 'success',
            'message' => $resourceName . ' updated',
        ]);
    }

    public function delete($resourceName, $id)
    {
        $modelName  = $resourceName . '_model';
        $model      = $this->$modelName;

        $model->delete($id);

        return jsonOutput($this, [
            'status' => 'success',
            'message' => $resourceName . ' deleted',
        ]);
    }

    public function renderPdf()
    {

        $this->load->library('pdfgenerator');
        $id = $this->session->userdata('user_id');


        $this->load->model('user_model');
        $data['user'] = $this->user_model->get($id);
        $data['experiences']    = $this->experience_model->getAll();
        $data['educations']     = $this->education_model->getAll();
        $data['hobbies']        = $this->hobby_model->getAll();
        $data['languages']      = $this->language_model->getAll();
        $data['skills']         = $this->skill_model->getAll();
        $data['socialmedias']   = $this->socialmedia_model->getAll();


        $filename = 'cv_' . $data['user']->name;
        $paper = 'A4';
        $orientation = "portrait";

        // dd(FULLPATH.'/assets/img/team-1.jpg');
        $html = $this->load->view('admin/cv', $data, true);
        $this->pdfgenerator->generate($html, $filename, $paper, $orientation);

        // $this->pdfgenerator->generateByUrl(base_url('admin/cv'), $filename,$paper,$orientation);

    }

    public function getTemplate($resource)
    {
    }

    public function import($resourceName)
    {
        if ($_FILES['file_import_' . $resourceName]['name'] == '') {
            jsonOutput($this, [
                'message' => 'validation-error',
                'errors' => [
                    'file_import_' . $resourceName => 'file_import_' . $resourceName . ' must be selected',
                ]
            ]);
        }

        $filename = $_FILES['file_import_' . $resourceName]['tmp_name'];
        $this->load->library('spreadsheet');
        $importArray = $this->spreadsheet->readToArray($filename);
        $modelName  = $resourceName . '_model';
        $model      = $this->$modelName;
        $primaryLabels = [
            'experience' => 'name',
            'education' => 'name',
            'language' => 'language',
            'hobby' => 'hobby',
            'skill' => 'name',
            'socialmedia' => 'name',
        ];

        $errors = [];

        if (count(array_intersect($importArray['header'], $model->columns))
             != count($model->columns)) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'sorry, please use the correct template file',
            ]);
        }

        
        foreach($importArray['data'] as $rowKey => $row) {
            foreach ($row as $cell) {
                if ($cell == '') {
                    unset($importArray['data'][$rowKey]);
                    $errors[] = $resourceName.' '.$row[$primaryLabels[$resourceName]].' not inserted because empty';
                    break;
                }
            }
        }

        if (!$model->storeBatch($importArray['data'])) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'sorry, we have a problem',
            ]);
        }

        return jsonOutput($this, [
            'status'    => 'success',
            'message'   => $resourceName . ' importing saved',
            'kicked_message'    => $errors
        ]);
    }

    public function export($resourceName)
    {
        $this->load->library('spreadsheet');
        $modelName    = $resourceName . '_model';
        $model        = $this->$modelName;
        $columnHead   = $model->columns;
        $data         = objectToArray($model->getAll(implode(', ', $columnHead)));

        $xlsxObject = $this->spreadsheet->getXlsx($data, $columnHead);
        header("Content-Type: application/vnd.ms-excel"); 
        header("Content-Disposition: attachment; filename=\"$resourceName\".xls"); 

        $xlsxObject->save('php://output');  
    }
}
