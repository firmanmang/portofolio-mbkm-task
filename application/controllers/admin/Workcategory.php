<?php

class Workcategory extends CI_Controller 
{
    public function __construct() {
        parent::__construct();
        $this->load->model('workcategory_model');
        $this->form_validation->set_rules('name', 'Work Name', 'required');

        if (! $this->session->has_userdata('user_id')) {
            $this->session->set_flashdata('message', 'Silahkan login dulu');
            redirect('auth/login');
        }
    }

    public function index()
    {
        $data['categories'] = $this->workcategory_model->getAll();
        $this->load->view('admin/work-category', $data);
    }

    public function getDatatable()
    {
        $params = $this->input->get();
        $data = $this->workcategory_model->getForDatatable($params);

        return jsonOutput($this, $data);
    }

    public function getAll()
    {
        $data = $this->workcategory_model->getAll();

        return jsonOutput($this, [
            'status'    => 'success',
            'data'      => $data,
        ]);
    }
    
    public function store()
    {
        if (! $this->form_validation->run()) {
            return jsonOutput($this, [
                'status' => 'validation-error',
                'errors' => $this->form_validation->error_array(),
            ]);
        }

        $data = [
            'name' => $this->input->post('name'),
        ];

        if (! $this->workcategory_model->store($data)) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'Sorry, failed to add new category',
            ]);
        }

        return jsonOutput($this, [
            'status' => 'success',
            'message' => $data['name'].' saved',
        ]);
    }

    public function show($id)
    {
        $category = $this->workcategory_model->get($id);

        if($category == null) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'Sorry, this category not found',
            ]);
        }

        return jsonOutput($this, [
            'status' => 'success',
            'data' => $category,
        ]);
    }

    public function update($id)
    {
        
        $category = $this->workcategory_model->get($id);

        if($category == null) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'Sorry, this category not found',
            ]);
        }

        if (! $this->form_validation->run()) {
            return jsonOutput($this, [
                'status' => 'validation-error',
                'errors' => $this->form_validation->error_array(),
            ]);
        }

        $data = inputToArrayAssoc($this->input->post(), $this->workcategory_model);

        if (! $this->workcategory_model->update($id, $data)) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'Sorry, failed update this category',
            ]);
        }

        return jsonOutput($this, [
            'status' => 'success',
            'message' => $data['name'].' saved',
        ]);
    }
}