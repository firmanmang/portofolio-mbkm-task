<?php

class Post extends CI_Controller 
{
    private $errorMessages;

    public function __construct() {
        parent::__construct();
        $this->load->model('post_model');

        if (! $this->session->has_userdata('user_id')) {
            $this->session->set_flashdata('message', 'Silahkan login dulu');
            redirect('auth/login');
        }
    }
    
    public function index()
    {
        $this->load->view('admin/post/index');
    }

    public function get($id)
    {
        $post = $this->post_model->get($id);
        return jsonOutput($this, [
            'status' => 'success',
            'data' => $post,
        ]);
    }
    
    
    public function getAll()
    {
        $params = $this->input->get();
        // $posts = $this->post_model->getGeneral();
        $posts = $this->post_model->getForDatatable($params);

        return jsonOutput($this, $posts);
    }

    public function show($id)
    {
        $data['post'] = $this->post_model->get($id);

        $this->load->view('admin/post/show', $data);
    }

    public function create()
    {
        $this->load->view('admin/post/create');
    }

    public function store()
    {
        if (! $this->form_validation->run()) {
            return jsonOutput($this, [
                'status' => 'validation-error',
                'errors' => $this->form_validation->error_array(),
            ]);
        }
        
        $imagePath = $this->uploadImage();

        if ($imagePath == null) {
            return jsonOutput($this, [
                'status' => 'validation-error',
                'errors' => $this->errorMessages,
            ]);
        }

        $inputData = $this->input->post();
        $inputData['img_feature'] = $imagePath;
        $inputData['created_by'] = $this->session->userdata('user_id');
        $postData = inputToArrayAssoc($inputData, $this->post_model);

        if (! $this->post_model->store($postData)) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'Post not saved',
            ]);
        }

        return jsonOutput($this, [
            'status' => 'success',
            'message' => $inputData['title'].' successfully saved',
        ]);
    }

    public function edit($id)
    {
        $data['post'] = $this->post_model->get($id);
        $this->load->view('admin/post/edit', $data);
    }

    public function update($id)
    {
        if (! $this->form_validation->run()) {
            return jsonOutput($this, [
                'status' => 'validation-error',
                'errors' => $this->form_validation->error_array(),
            ]);
        }

        $this->form_validation->set_rules('slug', 'Slug', ['required', [
                'slug_callable',
                function($str) use ($id) {
                    $posts = $this->post_model->findSlug($str);

                    foreach ($posts as $post) {
                        if ($post->slug == $str && $post->id != $id) {
                            $this->form_validation->set_message('slug_callable', 'The slug must be unique');
                            return false;
                        }
                    }

                    return true;
                }
            ]]);

        if (! $this->form_validation->run()) {
            return jsonOutput($this, [
                'status' => 'validation-error',
                'errors' => $this->form_validation->error_array(),
            ]);
        }

        $imagePath = $this->uploadEditImage();
        $inputData = $this->input->post();

        if ($imagePath != null) {
            $inputData['img_feature'] = $imagePath;
        }
        
        $postData = inputToArrayAssoc($inputData, $this->post_model);

        if (! $this->post_model->update($id, $postData)) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'Post not saved',
            ]);
        }

        return jsonOutput($this, [
            'status' => 'success',
            'message' => $inputData['title'].' successfully saved',
        ]);
    }


    public function delete($id)
    {
        $this->post_model->delete($id);
        return jsonOutput($this, [
            'status' => 'success',
            'message' => 'Successfully deleted',
        ]);
    }

    
    private function uploadImage()
    {
        $this->load->helper('string');
        
        $config['upload_path']   = './uploads/posts';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size']      = 1024;
        $config['file_name']     = random_string('alnum', 30);

        $this->load->library('upload', $config);

        if (! $this->upload->do_upload('img_feature')) {

            $this->errorMessages['img_feature'] = $this->upload->display_errors();

            return null;
        }

        $fullpath = $this->upload->data('full_path');
        $imagePath = str_replace(FCPATH, '/', $fullpath);

        return $imagePath;
    }

    private function uploadEditImage()
    {
        if ($_FILES['img_feature']['name'] == '') {
            return null;
        }
        
        $this->load->helper('string');
        
        $config['upload_path']   = './uploads/posts';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size']      = 1024;
        $config['file_name']     = random_string('alnum', 30);

        $this->load->library('upload', $config);

        if (! $this->upload->do_upload('img_feature')) {

            $this->errorMessages['img_feature'] = $this->upload->display_errors();

            return null;
        }

        $fullpath = $this->upload->data('full_path');
        $imagePath = str_replace(FCPATH, '/', $fullpath);

        return $imagePath;
    }
}