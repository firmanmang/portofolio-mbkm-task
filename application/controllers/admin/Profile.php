<?php

class Profile extends CI_Controller 
{
    private $id;
    private $errorMessages = [];

    public function __construct() {
        parent::__construct();

        $this->id = $this->session->userdata('user_id');
        $this->load->model('user_model');
		$this->load->model('provinsi_model');

        if (! $this->session->has_userdata('user_id')) {
            $this->session->set_flashdata('message', 'Silahkan login dulu');
            redirect('auth/login');
        }
	}

    public function index()
    {

        $this->load->model('education_model');
        $this->load->model('experience_model');
        $this->load->model('hobby_model');
        $this->load->model('language_model');
        $this->load->model('skill_model');
        $this->load->model('socialmedia_model');

		$data['experiences']    = $this->experience_model->getAll();
		$data['educations']     = $this->education_model->getAll();
        $data['hobbies']        = $this->hobby_model->getAll();
        $data['languages']      = $this->language_model->getAll();
        $data['skills']         = $this->skill_model->getAll();
        $data['socialmedias']   = $this->socialmedia_model->getAll();

        $data['provinsiList'] = $this->provinsi_model->getAll('id, nama');
        $data['user'] = $this->user_model->get($this->id);

        $this->load->view('admin/profile', $data);

    }

    public function changePassword()
    {
        if (! $this->form_validation->run('change-password')) {
            return jsonOutput($this, [
                'status' => 'validation-error',
                'errors' => $this->form_validation->error_array(),
            ]);
        }

        $data = $this->input->post();
        
        if (! $this->user_model->changePassword($this->id, $data)) {
            return jsonOutput($this, [
                'status' => 'validation-error',
                'errors' => [
                    'current_password' => 'Please check current password',
                ],
            ]);
        }

        return jsonOutput($this, [
            'status' => 'success',
            'message' => 'Password successfully changed',
        ]);
    }

    public function updateProfile()
    {
        if (! ($this->uploadImage() 
                && $this->updateInformationValidation())) {

            return jsonOutput($this, [
                'status' => 'validation-error',
                'errors' => $this->errorMessages,
            ]);
        }

        $inputData = $this->input->post();
        $userDataInput = inputToArrayAssoc($inputData, $this->user_model);
        $id = $this->session->userdata('user_id');
        
        if (! $this->user_model->update($id, $userDataInput)) {
            return jsonOutput($this, [
                'status' => 'error',
                'message' => 'Profile not change',
            ]);    
        }
        
        $user = $this->user_model->get($this->id);
        $this->session->set_userdata('username', $user->username);
        
        return jsonOutput($this, [
            'status' => 'success',
            'message' => 'Profile successfully changed',
            'data' => $user,
        ]);
    }

    private function uploadImage(): bool
    {
        if ($_FILES['image_profile']['name'] == '') {
            return true;
        }

        $this->load->helper('string');
        
        $config['upload_path']   = './uploads/avatars';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size']      = 1024;
        $config['file_name']     = random_string('alnum', 30);

        $this->load->library('upload', $config);

        if (! $this->upload->do_upload('image_profile')) {

            $this->errorMessages['image_profile'] = $this->upload->display_errors();

            return false;
        }

        $fullpath = $this->upload->data('full_path');
        $imagePath = str_replace(FCPATH, '/', $fullpath);

        $this->user_model->update($this->id, [
            'image_profile' => $imagePath
        ]);        

        return true;
    }

    private function updateInformationValidation(): bool
    {
      
        if (! $this->form_validation->run('update-profile')) {
            $this->errorMessages = $this->form_validation->error_array();
            return false;
        }

        $user = $this->user_model->get($this->id);
        
        $this->form_validation
            ->reset_validation()
            ->set_rules('username', 'Username', [['username_callable', 
                function($username) use ($user) {
                    if ($user->username != $username 
                        && $this->user_model->isExist('username', $username)) {
                            $this->form_validation
                                ->set_message('username_callable', 'Please use another {field}');
                            return false;
                    }

                    return true;
            }]])
            ->set_rules('email', 'Email', [['email_callable', 
                function($email) use ($user) {
                    if ($user->email != $email 
                        && $this->user_model->isExist('email', $email)) {
                            $this->form_validation
                                ->set_message('email_callable', 'Please use another {field}');
                            return false;
                    }

                    return true;
            }]]);
        
        
        if (! $this->form_validation->run()) {
            return false;
        }

        return true;
    }
}