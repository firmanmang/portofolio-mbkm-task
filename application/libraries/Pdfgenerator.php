<?php


use Dompdf\Dompdf;
use Dompdf\Options;

class Pdfgenerator {
    public function generate($html, $filename='', $paper = '', $orientation = '', $stream=TRUE)
    {   
        $options = new Options();
        $options->set('isRemoteEnabled', FALSE);
        $options->set('isHtml5ParserEnabled'. true);
        $options->setChroot(FCPATH);
        $dompdf = new Dompdf($options);
        $dompdf->setBasePath(base_url());
        $dompdf->loadHtml($html);
        $dompdf->setPaper($paper, $orientation);
        $dompdf->render();
        if ($stream) {
            $dompdf->stream($filename.".pdf", array("Attachment" => 0));
        } else {
            return $dompdf->output();
        }
    }

    public function generateByUrl($url, $filename='', $paper = 'A4', $orientation = 'portrait', $stream=TRUE)
    {   
        $options = new Options();
        $options->set('isRemoteEnabled', TRUE);
        $options->set('isHtml5ParserEnabled'. true);
        $dompdf = new Dompdf($options);
        $dompdf->setBasePath(base_url());
        $dompdf->loadHtmlFile($url);
        $dompdf->setPaper($paper, $orientation);
        $dompdf->render();
        if ($stream) {
            $dompdf->stream($filename.".pdf", array("Attachment" => 0));
        } else {
            return $dompdf->output();
        }
    }
}