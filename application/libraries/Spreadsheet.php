<?php

use \PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class Spreadsheet  
{
    public function readToArray($filename)
    {
        $spreadsheet = IOFactory::load($filename);
        $spreadsheet->setActiveSheetIndex(0);
        $worksheet = $spreadsheet->getActiveSheet();

        $columnIterator = $worksheet->getRowIterator(1)->current();
        $cellIterator = $columnIterator->getCellIterator();

        $columnsHead = [];
        $data = [];


        foreach ($cellIterator as $cell) {
            $columnsHead[] = str_replace(' ', '_' , $cell->getValue());
        }
        
        foreach ($worksheet->getRowIterator(2) as $key => $row) {
            $i = 0;
            foreach ($row->getCellIterator() as $cell) {
                $data[$key][$columnsHead[$i++]] = $cell->getValue();
            }
        }

        return [
            'header' => $columnsHead,
            'data' => array_values($data),
        ];
    }

    public function getXlsx($data, $columnsHead)
    {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet(); 
        $worksheet->fromArray($columnsHead, NULL);
        $worksheet->fromArray($data, NULL, 'A2');
        
        return new Xls($spreadsheet);
    }
}