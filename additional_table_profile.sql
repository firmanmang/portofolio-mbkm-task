DROP TABLE IF EXISTS experiences;
DROP TABLE IF EXISTS educations;
DROP TABLE IF EXISTS languages;
DROP TABLE IF EXISTS hobies;
DROP TABLE IF EXISTS skills;
DROP TABLE IF EXISTS social_medias;

CREATE TABLE experiences (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(100) NOT NULL,
    start_date DATE NOT NULL,
    resign_date DATE NOT NULL,
    description text,
    CONSTRAINT pk_experiences PRIMARY KEY (id)
);
CREATE TABLE educations (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(100) NOT NULL,
    start_date DATE NOT NULL,
    graduate_date DATE NOT NULL,
    description text,
    CONSTRAINT pk_educations PRIMARY KEY (id)
);
CREATE TABLE languages (
    id INT AUTO_INCREMENT NOT NULL,
    language VARCHAR(100) NOT NULL,
    level INT NOT NULL,
    CONSTRAINT pk_languages PRIMARY KEY (id)
);
CREATE TABLE hobies (
    id INT AUTO_INCREMENT NOT NULL,
    hobby VARCHAR(100) NOT NULL,
    CONSTRAINT pk_hobies PRIMARY KEY (id)
);
CREATE TABLE skills (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(100) NOT NULL,
    level INT NOT NULL,
    CONSTRAINT pk_skills PRIMARY KEY (id)
);
CREATE TABLE social_medias (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(100) NOT NULL,
    url VARCHAR(100) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    last_modified TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT pk_social_medias PRIMARY KEY (id)
);