 DROP TABLE IF EXISTS work_categories;
 DROP TABLE IF EXISTS works;
 DROP TABLE IF EXISTS posts;
 DROP TABLE IF EXISTS categories;
 DROP TABLE IF EXISTS post_categories;
 DROP TABLE IF EXISTS user;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image_profile` text,
  `phone_number` varchar(13) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(12) NOT NULL,
  `password` text NOT NULL,
  `provinsi_id` varchar(2) DEFAULT NULL,
  `kabupaten_id` varchar(4) DEFAULT NULL,
  `kecamatan_id` varchar(7) DEFAULT NULL,
  `desa_id` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE categories (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(100) NOT NULL,
    CONSTRAINT pk_categories PRIMARY KEY(id)
);

CREATE TABLE works (
    id INT AUTO_INCREMENT NOT NULL,
    work VARCHAR(255) NOT NULL,
    description text NOT NULL,
    year VARCHAR(4) NOT NULL,
    city VARCHAR(250) NOT NULL,
    position VARCHAR(100) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    last_modified TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT pk_works PRIMARY KEY(id)
);

CREATE TABLE work_categories (
    id INT AUTO_INCREMENT NOT NULL,
    work_id INT NOT NULL,
    category_id INT NOT NULL,
    CONSTRAINT pk_work_categories PRIMARY KEY(id),
    CONSTRAINT fk_work_id FOREIGN KEY (work_id)
        REFERENCES works(id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT fk_category_id FOREIGN KEY (category_id)
        REFERENCES categories(id) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE post_categories (
    id INT AUTO_INCREMENT NOT NULL,
    category_name VARCHAR(200) NOT NULL,
    is_active enum('0', '1'),
    CONSTRAINT pk_post_categories PRIMARY KEY(id)
);

CREATE TABLE posts (
    id INT AUTO_INCREMENT NOT NULL,
    title VARCHAR(200) NOT NULL,
    slug VARCHAR(200) NOT NULL,
    category_id INT NOT NULL,
    content text NOT NULL,
    img_feature text NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    created_by INT NOT NULL,
    last_modified TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT pk_posts PRIMARY KEY(id),
    CONSTRAINT fk_post_category_id FOREIGN KEY (category_id)
        REFERENCES post_categories(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT fk_user_id FOREIGN KEY (created_by)
        REFERENCES user(id) ON UPDATE CASCADE ON DELETE RESTRICT
);
