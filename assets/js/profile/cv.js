

// <!-- <li>
//     Lorem ipsum
//     <div class="mt-1 w-56 rounded-full bg-gray-200">
//         <div class="child w-[24%] rounded-full bg-gray-400 py-1"></div>
//     </div>
// </li> -->

let cv_fullname    = $('#cv_fullname')
let cv_address     = $('#cv_address')
let cv_email       = $('#cv_email')
let cv_phone       = $('#cv_phone')
let cv_experience  = $('#cv_experience')
let cv_education   = $('#cv_education')
let cv_hobby       = $('#cv_hobby')
let cv_language    = $('#cv_language')
let cv_skill       = $('#cv_skill')
let cv_socialmedia = $('#cv_socialmedia')

cv_fullname.text(fullname)
cv_email.text(email)
cv_phone.text(phone)


$.get(siteUrl + 'admin/cv/get-all/experience')
    .done(response => {
        for (i in response) {
            let experience  = response[i]
            let detail      = $('<div>')

            detail.append($('<h4>', {
                    class: 'mb-2 text-lg font-bold',
                    text: experience.name
                }))
                .append($('<p>', {
                    class: 'text-xs text-gray-400',
                    text: experience.description
                }))

            cv_experience.append(
                $('<div>', {
                    class: 'flex gap-9'
                })
                .append($('<span>', {
                        class: 'mb-2 mr-4 block min-w-56 border-r-2 text-xs',
                        text: experience.start_date + ' - ' + experience.resign_date
                }))
                .append(detail)
            )

        }
    });

$.get(siteUrl + 'admin/cv/get-all/education')
    .done(response => {
        for (i in response) {
            let education  = response[i]
            cv_education.append(
                $('<li>').append(
                    $('<div>', {
                        class: 'grid gap-0.4'
                    })
                    .append($('<p>', {
                        class: 'm-0 font-bold',
                        text: education.name
                    }))
                    .append($('<p>', {
                        class: 'm-0 text-xs',
                        text: education.start_date + ' - ' + education.graduate_date
                    }))
                    .append($('<p>', {
                        class: 'm-0 text-xs',
                        text: education.description
                    }))
                )
            )

        }
    })

$.get(siteUrl + 'admin/cv/get-all/hobby')
    .done(response => {
        for (i in response) {
            let hobby  = response[i]
            cv_hobby.append(
                $('<li>', {
                    class: 'capitalize',
                    text: hobby.hobby
                })
            )
        }
    })

$.get(siteUrl + 'admin/cv/get-all/language')
    .done(response => {
        for (i in response) {
            let language  = response[i]
            cv_language.append(
                $('<li>', {
                    text: language.language,
                    class: 'capitalize'
                })
                .append(
                    $('<div>', {
                        class: 'mt-1 w-56 rounded-full bg-gray-200'
                    })
                    .append($('<div>', {
                        class: `child rounded-full bg-gray-400 py-1`,
                        style: `width:${language.level}%`
                    }))
                )
            )
        }
    })

$.get(siteUrl + 'admin/cv/get-all/skill')
    .done(response => {
        for (i in response) {
            let skill  = response[i]
            cv_skill.append(
                $('<li>', {
                    text: skill.name,
                    class: 'capitalize'
                })
                .append(
                    $('<div>', {
                        class: 'mt-1 w-56 rounded-full bg-gray-200'
                    })
                    .append($('<div>', {
                        class: `child rounded-full bg-gray-400 py-1`,
                        style: `width:${skill.level}%`
                    }))
                )
            )
        }
    })

$.get(siteUrl + 'admin/cv/get-all/socialmedia')
    .done(response => {
        for (i in response) {
            let socialmedia  = response[i]
            cv_socialmedia.append(
                $('<li>', {
                    class: 'break-normal truncate',
                    text: socialmedia.name.toUpperCase() + ': ' + socialmedia.url.toLowerCase()
                })
            )
        }
    })

$('#generate_cv_btn').click(() => {
    const doc = new jsPDF();
    var elementHTML = $('#cv_section').html()

    console.log(elementHTML);

    var specialElementHandlers = {
        '#editor': function (element, renderer) {
            return true;
        }
    };
    doc.fromHTML(elementHTML, 15, 15, {
        'elementHandlers': specialElementHandlers
    }, function(bla){ doc.save('sample-document.pdf')});
    
    // Save the PDF
   ;

})