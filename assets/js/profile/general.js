
    $('#update-profile').submit(function(e) {
        var inputs = {
            name: $('#name'),
            username: $('#username'),
            email: $('#email'),
            phone_number: $('#phone_number'),
        }
        e.preventDefault();

        var form = $(this)[0];
        var formData = new FormData(form);
        $.ajax({
            type: 'POST',
            url: siteUrl + '/admin/profile/update-profile',
            enctype: 'multipart/form-data',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data, status) {
                resetErrorMessage(inputs);

                if (data.status == 'success') {
                    Toast.fire({
                        icon: 'success',
                        title: data.message,
                    })
                    $('#preview-img').attr('src', data.data.image_profile);
                } else if (data.status == 'validation-error') {
                    for (var key in data.errors) {
                        $('#' + key + '_error').html(data.errors[key]);
                    }

                } else {
                    Toast.fire({
                        icon: 'error',
                        title: 'Sorry, failed to update your profile',
                    })
                }
            },
            error: function() {
                Toast.fire({
                    icon: 'error',
                    title: 'Sorry, failed to update your profile',
                })
            }
        })
    });

    $('#change-password').submit(function(e) {

        $inputs = {
            current_password: $('#current_password'),
            new_password: $('#new_password'),
            retype_password: $('#retype_password'),
        }
        e.preventDefault();

        var inputData = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: siteUrl+'/admin/profile/change-password    ',
            data: inputData,
            success: function(data, status) {
                resetErrorMessage($inputs);
                if (data.status == 'validation-error') {
                    for (var key in data.errors) {
                        $('#' + key + '_error').html(data.errors[key]);
                    }
                } else if (data.status == 'success') {
                    Toast.fire({
                        icon: 'success',
                        title: data.message,
                    })
                }
            },
            error: function(jqXHR, status, errorThrown) {

            }
        })
    });

    var provinsiOption = $('#provinsi')
    var kabupatenOption = $('#kabupaten')
    var kecamatanOption = $('#kecamatan')
    var desaOption = $('#desa')

    provinsiOption.select2()
    kabupatenOption.select2()
    kecamatanOption.select2()
    desaOption.select2()

    if (user['provinsi_id'] != '') {
        provinsiOption.val(user['provinsi_id'])
        provinsiOption.trigger('change')
    }

    if (user['kabupaten_id'] != '') {
        getKabupatenOption()
    }

    if (user['kecamatan_id'] != '') {
        getKecamatanOption()
    }

    if (user['desa_id'] != '') {
        getDesaOption()
    }

    provinsiOption.change(e => {
        getKabupatenOption();
    });

    kabupatenOption.change(e => {
        getKecamatanOption();
    });

    kecamatanOption.change(e => {
        getDesaOption();
    });

    async function getKabupatenOption() {
        let provinsiId = provinsiOption.val()

        await $.get(wilayah + '/kabupaten/' + provinsiId)
            .done(response => {
                let kabupatenList = response.kabupaten

                kabupatenOption.html($('<option>', {
                    disabled: true,
                    selected: true,
                    value: '-- Pilih Kabupatan --'
                }))

                kecamatanOption.html($('<option>', {
                    disabled: true,
                    selected: true,
                    value: '-- Pilih kecamatan --'
                }))

                desaOption.html($('<option>', {
                    disabled: true,
                    selected: true,
                    value: '-- Pilih desa --'
                }))

                kabupatenList.forEach(kabupaten => {
                    kabupatenOption.append($('<option>', {
                        class: 'capitalize',
                        value: kabupaten.id,
                        text: kabupaten.nama,
                    }))
                })

                if (user['kabupaten_id'] != '') {
                    kabupatenOption.val(user['kabupaten_id'])
                    kabupatenOption.trigger('change')
                }
            })
            .fail(error => {
                Toast.fire('Sorry, we have problem');
            });
    }

    async function getKecamatanOption() {
        let kabupatenId = kabupatenOption.val()

        await $.get(wilayah + '/kecamatan/' + kabupatenId)
            .done(response => {
                let kecamatanList = response.kecamatan

                kecamatanOption.html($('<option>', {
                    disabled: true,
                    selected: true,
                    value: '-- Pilih kecamatan --'
                }))

                desaOption.html($('<option>', {
                    disabled: true,
                    selected: true,
                    value: '-- Pilih desa --'
                }))

                kecamatanList.forEach(kecamatan => {
                    kecamatanOption.append($('<option>', {
                        class: 'capitalize',
                        value: kecamatan.id,
                        text: kecamatan.nama,
                    }))
                })

                if (user['kecamatan_id'] != '') {
                    kecamatanOption.val(user['kecamatan_id'])
                    kecamatanOption.trigger('change')
                }


            })
            .fail(error => {
                Toast.fire('Sorry, we have problem');
            });
    }

    async function getDesaOption() {
        let kecamatanId = kecamatanOption.val()
        await $.get(wilayah + '/desa/' + kecamatanId)
            .done(response => {
                let desaList = response.desa

                desaOption.html($('<option>', {
                    disabled: true,
                    selected: true,
                    value: '-- Pilih Kabupatan --'
                }))

                desaList.forEach(desa => {
                    desaOption.append($('<option>', {
                        class: 'capitalize',
                        value: desa.id,
                        text: desa.nama,
                    }))
                })

                if (user['desa_id'] != '') {
                    desaOption.val(user['desa_id'])
                    desaOption.trigger('change')
                }
            })
            .fail(error => {
                Toast.fire('Sorry, we have problem');
            });
    }