var tabActive = $('[aria-selected=true]')
tabActive.addClass('border-b-2 border-blue-500 font-bold');
var contentActive = tabActive.attr('data-tabs-target')

$('.content-tab').addClass('hidden')
$(contentActive).removeClass('hidden')

$('.tab').click(function() {
    contentActive = $(this).attr('data-tabs-target')
    $('.tab').removeClass('border-b-2 border-blue-500 font-bold');
    $(this).addClass('border-b-2 border-blue-500 font-bold');
    
    $('.content-tab').addClass('hidden')
    $(contentActive).removeClass('hidden')
});