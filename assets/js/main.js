
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 4500,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})

function disableForm() {
    for (var key in inputs) {
        inputs[key].prop('disabled', true);
        inputs[key].addClass('bg-gray-300');
        inputs[key].addClass('hover:bg-gray-300');
        inputs[key].addClass('active:bg-gray-300');
        inputs[key].addClass('focus:bg-gray-300');
    }
}

function enableForm() {
    for (var key in inputs) {
        inputs[key].prop('disabled', false);
        inputs[key].removeClass('bg-gray-300');
        inputs[key].removeClass('hover:bg-gray-300');
        inputs[key].removeClass('active:bg-gray-300');
        inputs[key].removeClass('focus:bg-gray-300');
    }
}

function showLoadingIndicator() {
    $('#load-indicator').removeClass('hidden');
    $('#text-submit').text('Loading...');
}

function hideLoadingIndicator(name) {
    $('#load-indicator').addClass('hidden');
    $('#text-submit').text(name);

}

function resetErrorMessage(inputs){
    for(key in inputs) {
        $('#' + key + '_error').html('');
    }
}
class DataHelper {

    async getAll(url) {
        let resultData = null
        await $.ajax({
            type: 'GET',
            url: url,
            success: function(data) {
                resultData = data;
            }
        });

        return resultData;
    }

    async get(url) {
        let resultData = null
        await $.ajax({
            type: 'GET',
            url: url,
            success: function(data) {
                if (data.status == 'success') {
                    resultData = data.data;
                    
                } else {
                    Toast.fire({
                        icon: data.status,
                        title: data.message,
                    })
                }
            }
        });

        return resultData;
    }

    store(url, data, successCallback) {
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: successCallback,
            error: function() {
                Toast.fire({
                    icon: 'error',
                    title: 'Sorry, we got problem',
                })
            }
        });
    }


    update(url, data, successCallback) {
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: successCallback,
            error: function() {
                Toast.fire({
                    icon: 'error',
                    title: 'Sorry, we got problem',
                })
            }
        });
    }
}

function ajaxDoneCallback(response) {
    if (response.status == 'success') {
        toggleModal();
        Toast.fire({
            icon: response.status,
            title: response.message,
        })
    } else if (response.status == 'validation-error') {
        for (key in response.errors) {
            $('#' + key + '_error_modal').html(response.errors[key]);
        }
    }
}

function ajaxFailCallback() {
    Toast.fire({
        icon: 'error',
        title: 'Sorry, we have a problem'
    })
}