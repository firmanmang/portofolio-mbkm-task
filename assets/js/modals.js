// Navbar notifications modal

var modal_triggers = document.querySelectorAll("[modal-trigger]");
modal_triggers.forEach((modal_trigger) => {
  let modal_menu = modal_trigger.parentElement.querySelector("[modal-menu]");

  modal_trigger.addEventListener("click", function () {
    console.log('lol');
    // modal_menu.classList.toggle("opacity-0");
    modal_menu.classList.toggle("hidden");
    if (modal_trigger.getAttribute("aria-hidden") == "false") {
      modal_trigger.setAttribute("aria-hidden", "true");
    } else {
      modal_trigger.setAttribute("aria-hidden", "false");
    }
  });

  window.addEventListener("click", function (e) {
    modal_trigger.click();
  });
});
